<?php
namespace DC\CoreBundle\Services;
use Symfony\Bundle\TwigBundle\TwigEngine AS Templating;
use Waldo\DatatableBundle\Util\Datatable as DT;

/**
 * Service Datatables Site
 *
 */
class SiteDataTableService {
    private $datatable;
    private $templating;

    public function __construct(DT $datatable,Templating $templating) {
        $this->datatable = $datatable;
        $this->templating = $templating;
//        $this->rowController =$rowController;
    }

    public function makeDatatable() {
        return $this->datatable
            ->setSearch(true)
            ->setSearchFields(array(0))
            ->setEntity('DCCoreBundle:Site','s')

            ->setFields(
                array(
                    "domaine" => "s.domain",
                    "Catégorie" => "c.nom",
                    "Url Ancre " => "s.url",
                    "vignette"=> "p.path",
                    "Ancre" => "s.ancre",
                    "No Follow ?" => "s.isNoFollowAncre",
                    "Description" => "s.showDescShort",
                    "Créé le "=> "s.created",
                    "Actions"         => 's.id',
                    "_identifier_"  => 's.id'
                )
            )
            ->addJoin('s.mainPic', 'p', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
            ->addJoin('s.category', 'c', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
        ->setRenderer(
            function(&$data)
            {
                foreach ($data as $key => $value)
                {

                    if ($key == 3)                                      // 1 => address field
                    {
                        $data[$key] =
                            $this->templating
                                ->render(
                                    'DCCoreBundle:Site/Partials/DTSite:mainPic.html.twig',
                                    array('site' => $value)
                                );
                    }
                    if ($key == 5)                                      // 1 => address field
                    {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
                        $data[$key] =  $this->templating
                            ->render(
                                'DCCoreBundle:Site/Partials/DTSite:noFollow.html.twig',
                                array('noFollow' => $value)
                            );
                    }
                    if ($key == 6)                                      // 1 => address field
                    {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
                        $data[$key] =  $this->templating
                            ->render(
                                'DCCoreBundle:Site/Partials/DTSite:showShortDesc.html.twig',
                                array('showShortDesc' => $value)
                            );
                    }
                    if ($key == 7)                                      // 1 => address field
                    {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
                        $data[$key] =  $this->templating
                            ->render(
                                'DCCoreBundle:Site/Partials/DTSite:date.html.twig',
                                array('created' => $value)
                            );
                    }
                }
            }
        )
            ->setRenderers(
                array(
                    2 => array(
                        'view' => 'DCCoreBundle:Site/Partials/DTSite:urlAncre.html.twig',
                    ),
                    8 => array(
                        'view' => 'DCCoreBundle:Site/Partials/DTSite:actions.html.twig',
                    )
                )
            )
            ->setOrder("s.created", "desc")        ;                         // it's also possible to set the default order

            //->setHasAction(false);
    }
}