<?php
namespace DC\CoreBundle\Services;
use Symfony\Bundle\TwigBundle\TwigEngine AS Templating;
use Waldo\DatatableBundle\Util\Datatable as DT;


class RowDataTableService {


    private $datatable;
    private $templating;

    public function __construct(DT $datatable,Templating $templating) {
        $this->datatable = $datatable;
        $this->templating = $templating;
//        $this->rowController =$rowController;
    }
    /**
     * set datatable configs
     *
     * @return \Ali\DatatableBundle\Util\Datatable
     */
    public function makeDatatable($id_cat)
    {
//        $controller_instance = $this->rowController;

        return $this->datatable
            ->setEntity("DCCoreBundle:Row", "r")                          // replace "XXXMyBundle:Entity" by your entity
            ->setFields(
                array(
                    " "                => 'r.isComplet',
                    "Catégorie"        => 'c.nom',                          //      "label" => "alias.field_attribute_for_dql"
                    "Sites"            => 'r.id',
                    "Gabarit"          => 'g.format',                        // Declaration for fields:
                    "Créée le"         => 'r.created',
                    "Actions"         => 'r.id',
                    "_identifier_"  => 'r.id')                          // you have to put the identifier field without label. Do not replace the "_identifier_"
            )
            ->addJoin('r.gabarit', 'g', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
            ->addJoin('r.category', 'c', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
//            ->addJoin('r.siteRows', 'sr', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
            ->setWhere(                                                     // set your dql where statement
                'r.category = :cat',
                array('cat' => $id_cat)
            )

            ->setRenderer(

//                function(&$data) use ($controller_instance)
                function(&$data)
                {

                    foreach ($data as $key => $value)
                    {
                        if ($key == 0)                                      // 1 => address field
                        {
//                            $data[$key] = $controller_instance
                            $data[$key] =
                                $this->templating
                                    ->render(
                                        'DCCoreBundle:Row/Partials/DTRow:complet.html.twig',
                                        array('complet' => $value)
                                    );
                        }
//                        if ($key == 2)                                      // 1 => address field
//                        {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
//                                ->render(
//                                    'DCCoreBundle:Row/Partials/DTRow:sites.html.twig',
//                                    array('dt_obj' => $value)
//                                );
//                        }

                        if ($key == 3)                                      // 1 => address field
                        {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
                            $data[$key] = $this->templating
                                ->render(
                                    'DCCoreBundle:Row/Partials/DTRow:formatGabarit.html.twig',
                                    array('gabarit' => $value)
                                );
                        }
                        if ($key == 4)                                      // 1 => address field
                        {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
                            $data[$key] =  $this->templating
                                ->render(
                                    'DCCoreBundle:Row/Partials/DTRow:date.html.twig',
                                    array('created' => $value)
                                );
                        }
//                        if ($key == 5)                                      // 1 => address field
//                        {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
//                                ->render(
//                                    'DCCoreBundle:Row/Partials/DTRow:actions.html.twig',
//                                    array('row_id' => $value)
//                                );
//                        }
                    }
                }
            )
            ->setRenderers(
                array(
                    2 => array(
                        'view' => 'DCCoreBundle:Row/Partials/DTRow:sites.html.twig',
                    ),
                    5 => array(
                        'view' => 'DCCoreBundle:Row/Partials/DTRow:actions.html.twig',
                    )
                )

            )
            ->setOrder("r.isComplet", "asc")           ;                      // it's also possible to set the default order
            //->setHasAction(false);
//            ->setSearch(TRUE)
//            ->setSearchFields(array(2));                                           // you can disable action column from here by setting "false".
    }

}