<?php
namespace DC\CoreBundle\Services;

use Doctrine\ORM\EntityManager;
use DC\CoreBundle\Entity\Setting;

class SettingsService {

    protected $em;
    protected $settings;

    public function __construct(EntityManager $em) {
        $this->em = $em;
        $this->settings = $this->getSettings();
    }

    protected function getSettings() {
        $settings = $this->em->getRepository('DCCoreBundle:Setting')->find(1);
        return $settings;
    }

    public function siteNameShort() {
        $siteNameShort = $this->settings->getSiteNameShort();
        return $siteNameShort;
    }
    public function siteName() {
        $siteName = $this->settings->getSiteName();
        return $siteName;
    }

    public function fullUrl() {
        $fullUrl = $this->settings->getFullUrl();
        return $fullUrl;
    }
    public function slogan() {
        $slogan = $this->settings->getSiteSlogan();
        return $slogan;
    }

    public function metaTitre() {
        $metaTitle = $this->settings->getMetaTitle();
        return $metaTitle;
    }

    public function metaDescription() {
        $metaDescription = $this->settings->getMetaDescription();
        return $metaDescription;
    }

    public function metaKeywords() {
        $metaKeywords = $this->settings->getMetaKeywords();
        return $metaKeywords;
    }

    public function isNoIndex() {
        $isNoIndex = $this->settings->getIsNoIndex();
        return $isNoIndex;
    }

    public function gaCode() {
        $isNoIndex = $this->settings->getGaCode();
        return $isNoIndex;
    }
}