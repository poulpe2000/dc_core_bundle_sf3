<?php
namespace DC\CoreBundle\Services;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RouterInterface;

class SitemapService
{

    private $router;
    private $em;

    public function __construct(RouterInterface $router, EntityManager $em) {
        $this->router = $router;
        $this->em = $em;
    }

    public function makeSitemapCategory() {
        $urls = array();

        $cats_sidebar = $this->em->getRepository('DCCoreBundle:Taxonomy')->getCategoryPrincipale();
        foreach ($cats_sidebar as $c) {
            $loc = $this->router->generate('dc_front_view_by_main_cat', array('slug' => $c->getSlug()), true);
            $urls[] = array(
                'loc' => $loc,
                'lastmod'=>$c->getUpdated()->format('Y-m-d'),
                'changefreq'=>'monthly',
                'priority'=>1
            );
            if($c->getChildren() != null) {
                foreach($c->getChildren() as $cat_child) {
                    $loc = $this->router->generate('dc_front_view_by_main_cat', array('slug' => $cat_child->getSlug()), true);
                    $urls[] = array(
                        'loc' => $loc,
                        'lastmod'=>$cat_child->getUpdated()->format('Y-m-d'),
                        'changefreq'=>'monthly',
                        'priority'=>1
                    );
                }
            }
        }
        return $urls;
    }

}