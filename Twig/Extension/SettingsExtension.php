<?php
namespace DC\CoreBundle\Twig\Extension;

// Import du service lié qui récupère les settings dans la DB
use DC\CoreBundle\Services\SettingsService;

class SettingsExtension extends \Twig_Extension {
   protected $settings;

    public function __construct(SettingsService $settings) {
        $this->settings = $settings;
    }

    public function getGlobals() {
        return array('settings'=>$this->settings);
    }

    public function getName() {
        return 'settings';
    }
}