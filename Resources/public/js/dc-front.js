// Lazy Loading
$("img.lazy").lazyLoadXT({
	 srcAttr: 'data-src-lazy',
	 edgeY:  200,
	 forceLoad:true
});
// Très important, on resize la div à chaque scroll => permet de réparer le bug des images qui ne s'affichent pas sur le portable
$("#main").on('scroll', function(){ 
    $(window).resize();
    // alert('bi');
});

var listSites = [];
$("div.bottom-thumb h3").each(function() { listSites.push($(this).text()) });
var quotedCSV = '"' + listSites.join('", "') + '"';
//console.log(quotedCSV);

var uniqueSites = [];
$.each(listSites, function(i, el){
	if($.inArray(el, uniqueSites) === -1) uniqueSites.push(el);
});

var i;
for (i = 0; i < uniqueSites.length; ++i) {
	$('ul#list_sites').append('<li><a target="_blank" href="http://www.'+uniqueSites[i]+'">'+uniqueSites[i]+'</a></li>');
}




