$(document).ready(function(){
///////////////////////////////////////////////
//
// Affichage des lignes filtrée par catégorie
//
///////////////////////////////////////////////
//----------------------------------------/////
//
// Fonctions Gestion des lignes
//
///--------------------------------------/////


// Fctn AJAX Affichage des lignes filtrée par catégorie
function loadRowsByCat(id_cat) {
	var DT_route = Routing.generate('dc_admin_row_load_rows');
	
	$.post(DT_route,{id_cat:id_cat},function(data){
		$('.load_dt_rows').fadeIn();
		$('.load_dt_rows').html(data);
		
		$("#rows_by_cat").sortable();
		$('.save-order-ligne').fadeIn();
	});
};

// Fctn AJAX Sauvegarder l'ordre des lignes
function saveOrderLignes() {
    var id_cat = $('.id_cat').attr('id');
    //initialisation de la chaine contenant les id des items
    var elements = "";
 
    //parcours de la liste d'items
    $('#rows_by_cat li').each(function()
    {
      //console.log($(this).attr('id'));
      //pour chaque item, on concatene l'id dans la chaine
      elements += $(this).attr('id')+",";

    })
  var save_order_ligne_route = Routing.generate('dc_admin_rows_save_order');
  //on execute l'action, a laquelle on envoie en param la liste des id
  $.post(save_order_ligne_route, { elements: elements.substr(0, elements.length-1)}, function(data){
    $('.statut_order').html(data);
    $('.statut_order').fadeIn(function(){
    	$(this).fadeOut(2000);
    });
    loadRowsByCat(id_cat);
  });
};


function loadRowToEdit(id_row) {
	var dc_admin_row_edit = Routing.generate('dc_admin_row_edit');
	$('.load_row_edit-'+id_row).hide();
	$.post(dc_admin_row_edit,{id_row:id_row},function(data){
		$('.load_row_edit-'+id_row).html(data);
		$('.load_row_edit-'+id_row).slideToggle();
		$("#sites_rows_sortable").sortable();
	});
};
//--------------------------------------/////
//
// Gestion des lignes listener events
//
///------------------------------------/////

	// Retiter un site d'une ligne
	$('.padding').on('click','.remove_site_row',function(){
		var id_site_row = $(this).data('id');
		var id_row = $(this).data('id-row');
		var dc_admin_remove_site_row = Routing.generate('dc_admin_remove_site_row');
		$.post(dc_admin_remove_site_row,{id_site_row:id_site_row,id_row:id_row},function(){
            loadRowToEdit(id_row);
		});
	});


// Affichage des lignes de la catégorie cliquée
$('.padding').on('click','.cat_admin',function(){
	var id_cat = $(this).attr('id');
	$('.cat_admin').each(function(){
		$(this).removeClass('label-info');
		$(this).addClass('label-primary');
	})
	$(this).toggleClass('label-primary');
	$(this).toggleClass('label-info');
	loadRowsByCat(id_cat);
});

// Sauvegarder l'ordre des lignes
$('.padding').on('click','.save-order-ligne',function(e){
	e.preventDefault();
	saveOrderLignes();
});

// Modifier une ligne existante
$('.padding').on('click','.edit_row_btn',function(e){
	e.preventDefault();
	
	$(' + .close_row_btn',this).fadeIn();
	$('.lre').each(function(){
		$(this).html(" ");
		$(this).hide();
	})
	var id_row = $(this).attr('id');
	loadRowToEdit(id_row);
	
});
$('.padding').on('click','.close_row_btn',function(e){
	var id_row = $(this).attr('id');
	$(this).hide();

	$('.load_row_edit-'+id_row).fadeOut();
});

// Mettre online une ligne
$('.padding').on('click','.publish-site',function(e){
	e.preventDefault();
	var id_row = $(this).attr('id');
	var id_cat = $('.id_cat').attr('id');
	var publish_site_route = Routing.generate('dc_admin_publish_row',{ id: id_row })
	$.post(publish_site_route,function(){
		loadRowsByCat(id_cat);
		
	});
});

// Mettre offline une ligne
$('.padding').on('click','.offline-site',function(e){
	e.preventDefault();
	var id_row = $(this).attr('id');
	var id_cat = $('.id_cat').attr('id');
	var offline_site_route = Routing.generate('dc_admin_offline_row',{ id: id_row })
	$.post(offline_site_route,function(){
		loadRowsByCat(id_cat);
		
	});
});
// Modifier un site => Affichage slide avec zone modification d'un site
$('.padding').on('click','.edit_site_slide',function(e){
	e.preventDefault();
	// $('.edi')
	$('.slide_to_right').fadeOut();
	$('.edit_slide').fadeIn();
	$('.edit_slide').animate({
		"width":"75%"
	});
	$(".wait_edit").show();

	

});
// Revenir à la gestion des lignes
$('.wrapper').on('click','.come_back',function(){
	
	$('.slide_to_right').fadeIn();
	$('.edit_slide').hide('slow');
	$(".wait_edit").hide();
	
	dtable.ajax.reload();
});
///////////////////////////////////////////////
//
// Gérer les sites
//
///////////////////////////////////////////////
// Call datatables, and return the API to the variable for use in our code
// Binds datatables to all elements with a class of datatable
var dtable = $("#dta-unique-id_1").DataTable();
var dtablePub = $("#dta-unique-id_pub").DataTable();
$('.wrapper').on('click','.put-offline-btn',function(e){
	e.preventDefault();
	var id = $(this).attr('id');
	$(this).hide();
	var dc_admin_put_offline = Routing.generate('dc_admin_put_offline',{ id: id });
	$.post(dc_admin_put_offline,function(){
		
		$('.put-online-btn-'+id).show();
		dtable.ajax.reload();
	});
});
// Effacer la recherche sauvegardée par défaut dans DT
$(".wrapper").on('click','.del-search',function(e){
	e.preventDefault();
	dtable.state.clear();
	localStorage.removeItem("lastSearch");
	window.location.reload();
	
});

// Si recherche effectuée, on préremplie le champ en cas de reload de la page
var lastSearch = localStorage.getItem("lastSearch");

if(lastSearch !=null && lastSearch.length >=3){
	// alert(lastSearch);
	$("#dta-unique-id_1 input").val(lastSearch);
	$('#dta-unique-id_1 input').css("border",'2px solid red');
}


// Lancer la recherce au bout de 3 lettres saisies
$("#dta-unique-id_1 input")
    .unbind() // Unbind previous default bindings
    .bind("input", function(e) { 
// alert(this.value.length);
    // Bind our desired behavior
        // If the length is 3 or more characters, or the user pressed ENTER, search
        if(this.value.length >= 3 || e.keyCode == 13) {
        	// alert(dtable);
            // Call the API search function
            dtable.columns(0).search(this.value).draw();
            $('#dta-unique-id_1 input').css("border",'2px solid red');
            localStorage.setItem('lastSearch',this.value);
        }
        // Ensure we clear the search if they backspace far enough
        if(this.value == "") {
            dtable.search("").draw();
        }
        return;
    });



///////////////////////////////////////////////
//
// Ajouter un site
//
///////////////////////////////////////////////
//-------------------------/////
//
// Fonctions Gestion des lignes
//
///------------------------/////
// Verifier le format d'une url
function checkURLFormat(val) {
	
	var reg_http = new RegExp("^(http\:\/\/)");
	var reg_https = new RegExp("^(https\:\/\/)");
	var reg_url = new RegExp("^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$");
	var fail = true;
		if( reg_https.test(val) || reg_http.test(val) ) {
			fail = false;
		}
		if(fail){
			$('.url_error').html('Adresse URL incomplète !').show('slow');
			return true;
		} else {
			$('.url_error').hide('slow');
			return false;
		}
		
};
// Initialiser le plugin Chosen
function initChosenPlugin() {
	// Chosen plugin
 	var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    };
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    $("#dc_adminbundle_site_tags,#dc_adminbundle_text_tags").change(function(){
		    //console.log($(this).val());
		    var add_site_tags_listener = Routing.generate('dc_admin_add_site_tags_listener')
			  $.post(add_site_tags_listener, {ids_tags: $(this).val()}, function(data){
			    $("#dc_adminbundle_site_category, #dc_adminbundle_text_category").html(data);
			  });
			}); // fin .change
};
function loadGallery(id_site,small_thumbs) {
	var small_thumbs = small_thumbs
	var dc_admin_row_show_gallery = Routing.generate('dc_admin_row_show_gallery');
	$.post(dc_admin_row_show_gallery,{id_site:id_site,small_thumbs:small_thumbs},function(data){
		$('.load_gallery').html(data);
	});
};
// JCROP
function initJcrop(w,h){
	$('#crop').cropper({
		data: {x:100,y:100,width:w, height:h},
		resizable:false
	});
};
// Afficher les gabarits
function displayGabarits() {
	var load_gabrits_route = Routing.generate('dc_admin_dispaly_gabrits');
	$('.lignes_existantes').hide();
	$.post(load_gabrits_route).done(function(data){
			$('.load_gabrits').html(data);
			$('.load_gabrits').fadeIn('slow');
	});
};
//--------------------------------------/////
//	
// Gestion des sites listener events
//
///------------------------------------/////
// Verifier le format d'url
$('#dc_adminbundle_site_url').on('blur',function(){
	var val = $(this).val()
	checkURLFormat(val);
});

$('#dc_adminbundle_site_url').on('paste', function () {
  var element = $(this);	
  setTimeout(function () {
    var url = $(element).val();
    checkURLFormat(url);
  }, 100);
});

// Charger les catégories principales en fonction des tags sélectionnés
initChosenPlugin();
// Sauvegarder une image Croppée
$('.wrapper').on('click','.save-crop',function(){
// $('.save-crop').click(function(){
 var img_data = $("#crop").cropper("getDataURL");
 var site_id = $('.url').attr('id');
 var $this = $(this);
 var make_thumb_route = Routing.generate('dc_admin_make_thumb_crop');
 $.post(make_thumb_route,{img_data:img_data,site_id:site_id}).done(function(data){
 	$(".crop_reponse").fadeOut(3000).html(data);
 	loadGallery(site_id,false);
 });
});
// Générer une vignette
// $('.do_thumb').click(function(e){
	$('.wrapper').on('click','.do_thumb',function(e){
  e.preventDefault();
 
  var val = $(".url_thumb").val();
  var res = checkURLFormat(val);
	if(!res) {
	  //alert('bibi');
	  var id_site = $("input.url").attr('id');
	  var url = $("input.url").val();
	  $('.load_thumb').html("<div class='col-lg-6 alert alert-info'><h4>Vignette en cours de création</h4></div>");
	  var do_thumb_route= Routing.generate('dc_admin_make_thumb');
	  var dc_admin_refresh_btn_new_ligne = Routing.generate('dc_admin_refresh_btn_new_ligne');
	  $.post(do_thumb_route, {url:url,id_site:id_site},function(data){
	    $('.load_thumb').html(data);
	    initJcrop(500,500);
	    loadGallery(id_site,false);
	    $.post(dc_admin_refresh_btn_new_ligne,{id_site:id_site},function(data){
	    	$(".load_new_ligne_btn").html(data);
	    	// Empecher l'ajout à une ligne si pas de vignette faite
			$('.tooltip-ligne').tooltip('hide');
	    });
	  });
	} // fin if res
});//fin do_thumb
// Extraire les balises meta
$('.extract_meta').click(function(e){
	e.preventDefault();
	var extract_meta_route = Routing.generate('dc_admin_extract_meta_tags');
	var url = $("#dc_adminbundle_site_url").val();
	if(url.length < 1) {
		alert('merci de saisir une url');
		$("#dc_adminbundle_site_url").css({"border":"1px solid #FF0000"});
	} else {
		$("#dc_adminbundle_site_descShort").html("en cours d'extration");
		$.post(extract_meta_route,{url:url})
			.done(function(data){
			$("#dc_adminbundle_site_descShort").html(data);
			});
	}
});
// Initaliser JCROP
initJcrop(500,500);





// Recharger les sites de la ligne
function reloadRow(id_row){
	var dc_admin_row_edit = Routing.generate('dc_admin_row_edit');
	$('.load_row_edit-'+id_row).html('<div class="loader_edit"><img src="/bundles/dccore/images/spiffygif_152x152.gif"></div>');
	$.post(dc_admin_row_edit,{id_row:id_row},function(data){
					$('.load_row_edit-'+id_row).html(data);
					$('.load_row_edit-'+id_row).fadeIn();
					// var sites_rows_div =$(data).find('#sites_rows_sortable');
					$("#sites_rows_sortable").sortable();
					
				});// Fin Rechargement de la ligne avec ses sites
};

// Recharger la ligne avant qu'elle n'exite dans la base/
// Utile quand on modifie le site dans le contexte d'ajout d'une nouvelle ligne
function reloadSiteRow(id_site,id_gabarit) {
	var dc_admin_preview_row = Routing.generate('dc_admin_preview_row');
	$.post(dc_admin_preview_row,{id_gabarit:id_gabarit,id_site:id_site},function(data){
		$('.load_ligne').html(data);
		$(".load_ligne").fadeIn('slow');
	});
};

function reloadAddSiteRow(id_row,id_site) {
	var dc_admin_add_new_site_row = Routing.generate('dc_admin_add_new_site_row');
	$(".lignes_existantes").hide();
	$.post(dc_admin_add_new_site_row,{id_row:id_row,id_site:id_site},function(data){
		$(".load_ligne").html(data);
		$('.load_ligne').fadeIn();
		$("#sites_rows_sortable").sortable();
	});
};

// // Modifier un site
$('.wrapper').on('click','.edit_site_slide',function(){
// $('.add_site').click(function(){
	var id_site = $(this).data('id-site');
	var id_row =$(this).data('id-row');
	var id_gabarit = $(this).data('id-gabarit');
	var add_site = $(this).data('add-site');
// alert(id_gabarit);
	// Routes
	initChosenPlugin();
	var dc_admin_edit_site = Routing.generate('dc_admin_edit_site',{id:id_site});
		$('#load_form_site').load(dc_admin_edit_site,function(){
			// CallBack load
			// --> inititalisation du plugin Chosen 
			initChosenPlugin();
			
			// --> // Si changement de tags, changement du champ catégorie principale. 
				   //Elle ne peut être que parmis l'un des tags choisis
			$("#dc_adminbundle_site_tags").change(function(){
			    //console.log($(this).val());
			    var add_site_tags_listener = Routing.generate('dc_admin_add_site_tags_listener')
				  $.post(add_site_tags_listener, {ids_tags: $(this).val()}, function(data){
				    $("#dc_adminbundle_site_category").html(data);
				  });
			}); // fin .change
			// --> // Validation ajax du formulaire
			$('#form_edit_site').on('submit',function(e){
				e.preventDefault();
				
				var $this = $(this);
				// alert($this.attr('action'));
				$.ajax({
					url		: $this.attr('action'),
					method  : $this.attr('method'),
					data 	: $this.serialize(),
					success : function(html) {
						$('.slide_to_right').fadeIn();
						$('.edit_slide').hide('slow');
						$(".wait_edit").hide();
						dtable.ajax.reload();
						if($.isNumeric(id_gabarit)) {
							// alert('bb');
							reloadSiteRow(id_site,id_gabarit);
							
						} 
						else if(add_site ==true) {
							// alert('toi');
							reloadAddSiteRow(id_row,id_site);
						}
						else {
							// alert('yo');
							reloadRow(id_row);
						}
						
					} 
				})
			});
		}); // fin .Load
	});

// Validation AJAX du form d'upload
$(".wrapper").on('submit','#form_upload',function(){
	
	var id_site = $(this).data('id-site');
	var options = {
		// url: {{ path('dc_admin_add_site_thumb',{id:site.id}) }}
		url: Routing.generate('dc_admin_add_site_thumb',{id:id_site})
	};
		var dc_admin_refresh_btn_new_ligne = Routing.generate('dc_admin_refresh_btn_new_ligne');
    $(this).ajaxSubmit({
    			success: function(html) {
					$('.load_thumb').html(html);
	    			initJcrop(500,500);
	    			loadGallery(id_site,false);
	    			$.post(dc_admin_refresh_btn_new_ligne,{id_site:id_site},function(data){
	    	$(".load_new_ligne_btn").html(data);
	    	// Empecher l'ajout à une ligne si pas de vignette faite
$('.tooltip-ligne').tooltip('hide');
	    });

				}

    	}); 
    return false; 
});

// $('#form_upload').submit(function(){
	
// 	var $this = $(this);
// 	var id_site = $(this).data('id-site');
//     $(this).ajaxSubmit({
//     	success: function(html) {
// 					$('.load_thumb').html(html);
// 	    			initJcrop(500,500);
// 	    			loadGallery(id_site,false);
// 				}

//     	}); 
// return false;
// });





// Modifier la galerie
$('.wrapper').on('click','.edit_site_slide_gallery',function(e){
e.preventDefault();
	// $('.edi')
	$('.slide_to_right').fadeOut();
	$('.edit_slide').fadeIn();
	$('.edit_slide').animate({
		"width":"75%"
	});
	$(".wait_edit").show();
	var id_site = $(this).data('id-site');
	var dc_admin_add_site_thumb = Routing.generate('dc_admin_add_site_thumb',{id:id_site});
	$('#load_form_site').load(dc_admin_add_site_thumb+" #thumb_zone",function(){
		initJcrop(500,500);
		$(".add-row-btn").hide();

	});

});


// // Rechargement de la ligne avec ses sites
				
				
				






// Afficher les gabarits
$('.add_row').click(function(e){
	e.preventDefault();
	displayGabarits();
});

// Preview ligne quand on clique sur vignette gabarit
$('.load_gabrits').on('click','.preview_gabarit',function(){
	var id_gabarit = $(this).attr('id');
	var id_site = $(".site_id").attr('id');
	var dc_admin_preview_row = Routing.generate('dc_admin_preview_row');
	$.post(dc_admin_preview_row,{id_gabarit:id_gabarit,id_site:id_site},function(data){
		$('.load_ligne').html(data);
		$(".load_ligne").fadeIn('slow');
	});
});

// Générer les lignes du gabarits
$('.load_gabrits').on('click','.gabarit',function(){
	var dc_admin_add_rows_gabrits = Routing.generate('dc_admin_add_rows_gabrits');
	var id_cat = $('.cat_id').attr('id');
	var id_gabarit = $(this).attr('id');
	var id_site = $(".site_id").attr('id');

    $.post(dc_admin_add_rows_gabrits,{id_cat:id_cat,id_gabarit:id_gabarit,id_site:id_site},function(data){
    	$(".load_ligne").html(data);
    });
});

// Afficher la galerie de photos du site
$('.padding').on('click','.show_gallery',function(e){
	e.preventDefault();
	var id_site = $(this).attr('id');
	var dc_admin_row_show_gallery = Routing.generate('dc_admin_row_show_gallery');
	$.post(dc_admin_row_show_gallery,{id_site:id_site},function(data){
		$('.load_gallery'+id_site).html(data);
		$('.load_gallery'+id_site).slideToggle();
		// $(".show_gallery").html('<a class="close-gallery" href="#">Masquer Galerie <i class="fa fa-times"></i></a>');
	});
});

// Ecouter l'évenement changement de couleur
$(".wrapper").on('click','.color_box',function(){

	var id_site = $(this).attr('id');
	var id_color = $(this).attr('title');
	var name_color = $('.color_name_'+id_color).attr('id');
	
	// alert($("#color_"+id_site).attr('class'));
	$("#color_"+id_site).removeClass().addClass("thumb "+name_color);
	$(".color_selected_"+id_site).attr('id',id_color);
});

// Ecouter l'évenement changement d'image depuis la gallery sur la page visualiser ligne
$(".wrapper").on('click','.pic_gallery',function(){
	var id_pic = $(this).attr('id');
	var thumb_gallery_p = false;
	// var id_site = $(".site_id").attr('id');
	var id_site = $(this).data('id-site');
	var is_cropped = $(this).attr('alt');
	var dc_admin_change_main_pic = Routing.generate('dc_admin_change_main_pic');
	$.post(dc_admin_change_main_pic,{id_pic:id_pic,id_site:id_site,thumb_gallery_p:thumb_gallery_p},function(d){
		$('.load_pic_'+id_site).html(d);
		if(is_cropped != 1) {
			initJcrop(500,500);
		} 
	});
});
// Ecouter l'évenement changement d'image depuis la gallery sur la page gérer la gallery
$('.wrapper').on('click','.pic_gallery_gestion',function(){
	var id_pic = $(this).attr('id');
	var thumb_gallery_p = true;
	var id_site = $(this).data('id-site');
	var is_cropped = $(this).attr('alt');
	var dc_admin_change_main_pic = Routing.generate('dc_admin_change_main_pic');
	$.post(dc_admin_change_main_pic,{id_pic:id_pic,id_site:id_site,thumb_gallery_p:thumb_gallery_p},function(d){
		$('.load_pic_gallery'+id_site).html(d);
		if(is_cropped != 1) {
			initJcrop(500,500);
		} 
	});
});

// Supprimer une vignette
$('.wrapper').on('click','.delete-pic',function(e){

	e.preventDefault();
	var id_pic = $(this).data('id-pic');
	var id_site = $(this).data('id-site');
	var dc_admin_delete_pic = Routing.generate('dc_admin_delete_pic',{id:id_pic});
	if(confirm("Supprimer ?")) {
		$.post(dc_admin_delete_pic,function(data){
			$('.reponse_delete').fadeIn();
			$('.reponse_delete').html(data);
			loadGallery(id_site,false);
		});
	}
});



// Enregistrer une ligne
$("#add_rows").on('click','.save-ligne',function(){
	var dc_admin_new_row_save = Routing.generate('dc_admin_new_row_save');
	$(".site_loaded").each(function(){
		var id_site = $(this).attr('id');
		var id_cat = $('.cat_id').attr('id');
		var nb_cols = $('.nb_cols_'+id_site).attr('id');
		var id_gabarit = $('.gabarit_row_preview').attr('id');
		var id_color = $(".color_selected_"+id_site).attr('id');
		var response = "";
		$.post(dc_admin_new_row_save,
			{id_site:id_site,
			id_cat:id_cat,
			nb_cols:nb_cols,
			id_gabarit:id_gabarit,
			id_color:id_color},
			function(data){
				response += data;
				$('.reponse_save_ligne').html(data);
				$('.reponse_save_ligne').slideToggle();
				$('.save-ligne').hide();
				$('.btn-new-site').show();
				$('.btn-new-texte').show();
			}); // Fin Post
	}); // Fin each
});
// Prévisualiser une ligne existante
$('.padding').on('click','.add_site_row',function(e){
	e.preventDefault();
	var id_row = $(this).attr('id');
	var id_site = $('.site_id').attr('id');
	var dc_admin_add_new_site_row = Routing.generate('dc_admin_add_new_site_row');
	$(".lignes_existantes").hide();
	$.post(dc_admin_add_new_site_row,{id_row:id_row,id_site:id_site},function(data){
		$(".load_ligne").html(data);
		$('.load_ligne').fadeIn();
		$("#sites_rows_sortable").sortable();
	});
});

// // Sauvegarder l'ordre des sites dans la ligne
// function saveOrderSitesRow(elements){
// 	//initialisation de la chaine contenant les id des items
//     // var elements = "";
//     // $("#sites_rows_sortable site_row").each(function(){
//     // 	 elements += $(this).attr('id')+",";
//     // });
//     var save_order_site_row_route = Routing.generate('dc_admin_rows_save_order_site_row');
//     $.post(save_order_site_row_route,{elements: elements.substr(0, elements.length-1)},function(data){
// $('.reponse_save_ligne').html("L'ordre des sites a été sauvegardé.");
//     });
// };


// Sauvegarder une ligne existante
$('.wrapper').on('click','.save-ligne-existant',function(){
	var id_row = $('.id_row').attr('id');
	var dc_admin_add_new_site_row_save = Routing.generate('dc_admin_add_new_site_row_save');
	var dc_admin_delete_sites_row = Routing.generate('dc_admin_delete_sites_row');
	// Etape 1 -> on supprime toutes les lignes
	
    
		$.post(dc_admin_delete_sites_row,{id_row:id_row},function(){
			// Etape 2 -> on les enregistre de nouveau (plus facile pour faire les mises à jour)
			var i = 0;
			$(".site_loaded").each(function(){
				i+=1;
				var id_site = $(this).attr('id');
				var id_row = $('.id_row').attr('id');
				var nb_cols = $('.nb_cols_'+id_site).attr('id');
				var id_color = $(".color_selected_"+id_site).attr('id');
				var position = i;
				
				var response = "";
					$.post(dc_admin_add_new_site_row_save,
						{id_site:id_site,
						id_row:id_row,
						nb_cols:nb_cols,
						id_color:id_color,
						position:position
					    },
						function(data){
							
							// $('.save-ligne-existant').hide();
							$('.btn-new-site').show();
							$('.btn-new-texte').show();
							$('.btn-publish_row').show();
							$('.btn-unpublish_row').show();
							
							// // Sauvegarde de l'ordre des lignes
							// if(elements.length > 0) {
							// 	saveOrderSitesRow(elements);
							// }
						}); // Fin Post
				}); // Fin each
				$('.reponse_save_ligne').html("La ligne a été sauvegardée.");
				$(".reponse_save_ligne").fadeIn("fast", function() { $(this).delay(1000).fadeOut("slow"); });
				
				

			}); // Fin Post delete
	
}); // Fin save Ligne 

// Publier la ligne
$('.wrapper').on('click','.btn-publish_row',function(){
	var id_row = $(this).attr('id');
	$(this).hide();
	var publish_site_route = Routing.generate('dc_admin_publish_row',{ id: id_row })
	$.post(publish_site_route,function(){
		$('.reponse_save_ligne').html("La ligne a été publiée.");
				$(".reponse_save_ligne").fadeIn("fast", function() { $(this).delay(1000).fadeOut("slow"); });
		$('.btn-unpublish_row').show();
		
	});

});
// Offline la ligne
$('.wrapper').on('click','.btn-unpublish_row',function(){
	var id_row = $(this).attr('id');
	$(this).hide();
	var dc_admin_offline_row = Routing.generate('dc_admin_offline_row',{ id: id_row })
	$.post(dc_admin_offline_row,function(){
		$('.reponse_save_ligne').html("La ligne est offline.");
				$(".reponse_save_ligne").fadeIn("fast", function() { $(this).delay(1000).fadeOut("slow"); });
		$('.btn-publish_row').show();
		
	});

});


// autocomplete url
	var cache = {};
	$("input[data-id=url_autoc]").autocomplete({
		source: function (request, response)
		{
			//Si la réponse est dans le cache
			if (request.term in cache)
			{
				response($.map(cache[request.term], function (item)
				{
					return {
						label: item.url,
						value: function ()
						{
							$('input[data-id=url_autoc]').val(item.url);
							return item.url;
						}
					};
				}));
			}
			//Sinon -> Requete Ajax
			else
			{
		            var objData = {};
		            var url_action_sf2 = $(this.element).attr('data-url');
		            	objData = { url: request.term };
				$.ajax({
					url: url_action_sf2,
					dataType: "json",
					data : objData,
					type: 'POST',
					success: function (data)
					{
						//Ajout de reponse dans le cache
						cache[request.term] = data;
						response($.map(data, function (item)
						{
							return {
								label: item.url,
								value: function ()
								{
										$('input[data-id=url_autoc]').val(item.url);
										return item.url;
								}
							};
						}));
					},
					error: function (jqXHR, textStatus, errorThrown)
					{
						console.log(textStatus, errorThrown);
					}
				});
			}
		},
		minLength: 3,
		delay: 300
	});
// Fin ajax autocomplete url


    ///////////////////////////////////////////////
//----------------------------------------/////
//
// Fonctions Gestion des pubs
//
///--------------------------------------/////
    $('.padding').on('click','.add_new_pub',function(e){
		e.preventDefault();
		var id_cat = $(this).data('id-cat');
		var dc_admin_attribuer_pub_load_form_ajax = Routing.generate('dc_admin_attribuer_pub_load_form_ajax',{id:id_cat});
        $.post(dc_admin_attribuer_pub_load_form_ajax,function(data){


        	$('.load_interface_attribution_pub').html(data);
            var wasVisible = $('.load_interface_attribution_pub').is(":visible");
            $("[id^=element]:visible").stop().slideUp("slow");
            if (!wasVisible) {
                $('.load_interface_attribution_pub').slideDown("slow");
            }
        	// $('.load_interface_attribution_pub').slideToggle();

			//$(".toggle-btn",data).bootstrapToggle();
		});

	});


    $('.padding').on('click','.switch',function(){

		var id_pub_details = $(this).data("id-pub-detail");
		var is_online = $("#id-pub-detail"+id_pub_details).val();
		// console.log($("#id-pub-detail"+id_pub_details).val());
		var dc_admin_switch_online_pub_ajax = Routing.generate('dc_admin_switch_online_pub_ajax',{id_pub_details:id_pub_details,is_online:is_online});
		//console.log(id_pub_details);
        $.post(dc_admin_switch_online_pub_ajax);

	});

    $('.padding').on('click','.save_url',function(e){
    	e.preventDefault();
        var id_pub_details = $(this).data("id-pub-detail");
        var url = $('#url_destination'+id_pub_details).val();

        var dc_admin_save_url_pub_ajax =  Routing.generate("dc_admin_save_url_pub_ajax",{id_pub_details:id_pub_details,url:url});
        $.post(dc_admin_save_url_pub_ajax,function(data){
            $('.reponse_ajax'+id_pub_details).html(data);
            $('.reponse_ajax'+id_pub_details).fadeIn("fast", function() { $(this).delay(1000).fadeOut("slow"); });
		});
	});

    $('.padding').on('click','.save_attribution_pub',function(e){
		e.preventDefault();
		//$(this).html('<img src="/bundles/dccore/images/animal.gif">');
		var id_pub = $('input[name=pub_thumb]:checked').val();
		var is_online = $('input[name=is_online]:checked').val();

		//console.log(is_online);
		var url_destination = $("#url_destination").val();
		var id_cat = $("#id_cat").data('id');
		// console.log(id_pub);
		// console.log(is_online);
		// console.log(url_destination);
		// console.log("id_cat: "+ id_cat);
        var dc_admin_attribuer_pub_validation = Routing.generate('dc_admin_attribuer_pub_validation');

        if($('input[name=pub_thumb]').val().length === 0 || url_destination.length === 0) {
        	alert('tous les champs sont obligatoires');
        	return false;
		}


        $.post(dc_admin_attribuer_pub_validation,{id_pub:id_pub,is_online:is_online,url_destination:url_destination,id_cat:id_cat},function(){
            $('.load_interface_attribution_pub').slideToggle();
        	loadGestionPubAjax();
		});

	});

	// Retirer une pub d'une catégorie
	$('.padding').on('click','.retrait_pub',function(e){
		e.preventDefault();
		var id = $(this).data('id-pub-details');
        var dc_admin_supprimer_pub_validation = Routing.generate('dc_admin_supprimer_pub_validation',{id:id});
        $.post(dc_admin_supprimer_pub_validation,function(data){
            $('.reponse_ajax').html(data);
            loadGestionPubAjax();
		});

    });

	loadGestionPubAjax();
	function loadGestionPubAjax() {
        var id_cat = $("#id_cat").data('id');
        var dc_admin_attribuer_gestion_pubs_ajax = Routing.generate('dc_admin_attribuer_gestion_pubs_ajax',{id_cat:id_cat});
        $(".load_interface_gestion_pub").html('<img src="/bundles/dccore/images/animal.gif">');
        $.post(dc_admin_attribuer_gestion_pubs_ajax,function(data){

            $('.load_interface_gestion_pub').html(data);
            $('.load_interface_gestion_pub').show();
        });
	}






}); // Fin document Ready