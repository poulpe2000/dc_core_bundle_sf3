$(document).ready(function(){/* off-canvas sidebar toggle */

// SideBar Responsive
$('[data-toggle=offcanvas]').click(function() {
  	$(this).toggleClass('visible-xs text-center');
    $(this).find('i').toggleClass('glyphicon-chevron-right glyphicon-chevron-left');
    $('.row-offcanvas').toggleClass('active');
    $('#lg-menu').toggleClass('hidden-xs').toggleClass('visible-xs');
    $('#xs-menu').toggleClass('visible-xs').toggleClass('hidden-xs');
    $('#btnShow').toggle();
});
// Menu déroulant
$("#menu").metisMenu();
    // setTimeout(showPub, 2000);
    //
    // function showPub() {
    //
    //     $( "div.pub" ).slideDown( "slow" );
    // }

    // Rotation des pubs
    var pubs = $('.pub');
    current = 0;
    pubs.hide();
    Rotator();
    function Rotator() {
        $(pubs[current]).fadeIn('slow').delay(6000).fadeOut('slow');
        $(pubs[current]).queue(function() {
            current = current < pubs.length - 1 ? current + 1 : 0;
            Rotator();
            $(this).dequeue();
        });
    }

    // Rotation des pubs Mobile
    var pubsMobile = $('.pubMobile');
    current = 0;
    pubsMobile.hide();
    RotatorMobile();
    function RotatorMobile() {
        $(pubsMobile[current]).fadeIn('slow').delay(6000).fadeOut('slow');
        $(pubsMobile[current]).queue(function() {
            current = current < pubsMobile.length - 1 ? current + 1 : 0;
            RotatorMobile();
            $(this).dequeue();
        });
    }

    // Nouveau menu





// Afficher Description du site
// $('.padding').on('mouseover','.show_desc',function(e){
// e.preventDefault();
// 	var id_site = $(this).attr('id');
// 	$('.btt_'+id_site).slideToggle("slow");
// });  


//--------------------------------------/////
//
// LABS -> Test code
//
///------------------------------------/////
// // Navigation par #
// window.onhashchange = function(){
//     var what_to_do = document.location.hash;    
//     if (what_to_do=="#add_row")
//         addRow();
// }

// Charger des images lentemement en fonction du scroll
// jQuery("img.lazy").Lazy({
// 	effect: "fadeIn",
//         effectTime: 1500,
//         threshold: 600,
      
// });

// $('img.lazy').bttrlazyloading();



// Gérer les lignes
// $("#rows_table").dataTable();

// $('.padding').on('click','.cat_admin',function(){
// 	var id_cat = $(this).attr('id');
// 	var DT_route = Routing.generate('dc_admin_row_results_dt');
// 	$.post(DT_route,{id_cat:id_cat},function(data){
// 		$('.load_dt_rows').html(data);
// 		$('.load_dt_rows').fadeIn();
// 	})
// })





// Formulaire ajouter un site

// Recherche de sites en doublon
// $('#dc_adminbundle_site_url').autocompleter({url_list: '/admin/site/search/url', url_get: '/admin/site/get/url'});



// // Masquer la gallery
// $('.load_ligne').on('click','.close-gallery',function(e){
// 	e.preventDefault();
// 	var id_site = $(this).attr('id');
// 	$('.load_gallery'+id_site).hide("slow",function(){
// 		$('.load_gallery'+id_site).html("");
// 		$(".show_gallery").html(' <a class="show_gallery" href="">Voir Galerie <i class="fa fa-picture-o"></i></a>');
// 		$('.load_gallery'+id_site).show();

// 	});
// });


// // Afficher les lignes existantes
// $(".padding").on('click','.add_row_existante',function(){
// 	$(".load_gabrits").hide();
// 	$(".load_ligne").hide();

// 	var cat_id = $('.cat_id').attr('id');
// 	var show_lignes_route = Routing.generate('dc_admin_rows',{ id: cat_id });
// 	$.post(show_lignes_route,function(data){

// 		$(".lignes_existantes").html(data);
// 		$('.lignes_existantes').slideToggle();
// 	});
// });



}); // Fin document Ready
