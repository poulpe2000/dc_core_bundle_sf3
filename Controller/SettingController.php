<?php
namespace DC\CoreBundle\Controller;

use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use \Symfony\Component\Console\Input\ArgvInput;
use \Symfony\Bundle\FrameworkBundle\Console\Application;
use DC\CoreBundle\Entity\Setting;
use DC\CoreBundle\Form\SettingType;
use DC\CoreBundle\Form\FormHandler;

class SettingController extends Controller {

    /**
     * @Template
     */
    public function manageAction(Request $request) {
        $em = $this->get('doctrine')->getManager();
        $settings = $em->getRepository("DCCoreBundle:Setting")->find(1);

        if(count($settings) !=1 ) {
            $settings = new Setting();
        }
        $form = $this->createForm(SettingType::class,$settings);
        $em= $this->getDoctrine()->getManager();
        $formHandler = new FormHandler($form,$request,$em);
        if ($formHandler->process()) {
            $this->get('session')->getFlashBag()->add('success',"Configuration prise en compte");
//            // Création du fichier de config générale de l'annuaire
//            $array = array(
//                'site_name' => $settings->getSiteName(),
//                'site_name_short' => $settings->getSiteNameShort(),
//                'site_slogan'=> $settings->getSiteSlogan(),
//                'meta_titre' => $settings->getMetaTitle(),
//                'meta_description' => $settings->getMetaDescription(),
//                'meta_keywords' => $settings->getMetaKeywords(),
//                'full_url' => $settings->getFullUrl()
//            );
//
//            //$dumper = new Dumper();
//
//            //$yaml = $dumper->dump($array);
//
//            //$path = $this->container->getParameter('kernel.root_dir');
//            //$path.='/config/directory.yml';
//            //file_put_contents($path, "parameters: ".$yaml);
//            $this->get('twig')->addGlobal('site_name_global', "toto");
//            $this->get('twig')->addGlobal('site_name_short_global', $settings->getSiteNameShort());
//            $this->get('twig')->addGlobal('site_slogan_global', $settings->getSiteSlogan());
//            $this->get('twig')->addGlobal('meta_titre_global', $settings->getMetaTitle());
//            $this->get('twig')->addGlobal('meta_description_global', $settings->getMetaDescription());
//            $this->get('twig')->addGlobal('meta_keywords_global', $settings->getMetaKeywords());
//            $this->get('twig')->addGlobal('full_url_global', $settings->getFullUrl());

            return $this->redirect($this->generateUrl('dc_admin_manage_setting'));
        }

        return array('form'=>$form->createView());


    }
}