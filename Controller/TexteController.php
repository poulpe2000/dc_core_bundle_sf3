<?php
namespace DC\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DomCrawler\Form;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use DC\CoreBundle\Entity\Text;
use DC\CoreBundle\Form\TextType;
use DC\CoreBundle\Form\FormHandler;



class TexteController extends Controller {
    public  function listTexteAction(){

    }

    /**
     * @Template
     */
    public function addTexteAction(Request $request){
        $text = new Text();
        $form = $this->createForm(TextType::class,$text);
        $em = $this->getDoctrine()->getManager();
        $formHandler = new FormHandler($form,$request,$em);
        return array('form'=>$form->createView());
    }
}