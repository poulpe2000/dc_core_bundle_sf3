<?php
/**
 * Created by PhpStorm.
 * User: poulpe
 * Date: 02/05/2017
 * Time: 15:54
 */

namespace DC\CoreBundle\Controller;

use DC\CoreBundle\Entity\Pub;
use DC\CoreBundle\Entity\PubDetails;
use DC\CoreBundle\Entity\Taxonomy;
use DC\CoreBundle\Form\FormHandler;
use DC\CoreBundle\Form\PubType;
use Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class PubController extends Controller
{

    /**
     * @Template()
     */
    public function addPubAction(Request $request){
       $pub = new Pub();
        $form = $this->createForm(PubType::class,$pub);
        $em = $this->getDoctrine()->getManager();
        $formHanlder = new FormHandler($form,$request,$em);
        if($formHanlder->process()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($pub);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success',"site ajoutée");
            return $this->redirect($this->generateUrl('dc_admin_pub_results_dt_new'));
        }
        return array('form'=>$form->createView());
    }


    public function editPubAction(Pub $pub,Request $request){

        $form = $this->createForm(PubType::class,$pub);
        $em = $this->getDoctrine()->getManager();
        $formHanlder = new FormHandler($form,$request,$em);
        if($formHanlder->process()) {

            $em = $this->getDoctrine()->getManager();
            $pub->getPicPub()->setUpdateRand(uniqid());
            $em->persist($pub);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success',"site modifé");
            return $this->redirect($this->generateUrl('dc_admin_pub_results_dt_new'));
        }
        return $this->render('DCCoreBundle:Pub:addPub.html.twig',array('form'=>$form->createView()) );
    }

    /**
     * @Template()
     */
    public function manageAction(){

    }

    public function datatable() {
        $controller_instance = $this;
        $datatable = $this->get('datatable');
        return $datatable
            ->setSearch(true)
            ->setSearchFields(array(0))
            ->setEntity('DCCoreBundle:Pub','p')

            ->setFields(
                array(
                    "nom" => "p.nom",
                    "vignette"=> "p.id",
                    "categories"=>'p.id',

//                    "Créé le "=> "p.created",
                    "Actions"         => 'p.id',
                    "Suppression"         => 'p.id',
                    "_identifier_"  => 'p.id'
                )
            )
            ->addJoin('p.picPub', 'pic', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)

            ->setRenderer(
                function(&$data) use ($controller_instance)
                {
                    foreach ($data as $key => $value)
                    {
//                        if ($key == 2)                                      // 1 => address field
//                        {
//                            $data[$key] =
//                                $controller_instance->get('templating')
//                                    ->render(
//                                        'DCCoreBundle:Pub/Partials/DTPub:listCategories.html.twig',
//                                        array('categories' => $value)
//                                    );
//                        }

//                        if ($key == 1)                                      // 1 => address field
//                        {
//                            $data[$key] =
//                                $controller_instance->get('templating')
//                                    ->render(
//                                        'DCCoreBundle:Pub/Partials/DTPub:pubPic.html.twig',
//                                        array('pubPath' => $value)
//                                    );
//                        }


//                        if ($key == 7)                                      // 1 => address field
//                        {
////                            $data[$key] = $controller_instance
////                                ->get('templating')
//                            $data[$key] =  $controller_instance->get('templating')
//                                ->render(
//                                    'DCCoreBundle:Pub/Partials/DTPub:date.html.twig',
//                                    array('created' => $value)
//                                );
//                        }
                    }
                }
            )
            ->setRenderers(
//
                array(
                    1 => array(
                        'view' => 'DCCoreBundle:Pub/Partials/DTPub:pubPic.html.twig',
                    ),
                    2 => array(
                        'view' => 'DCCoreBundle:Pub/Partials/DTPub:listCategories.html.twig',
                    ),
                    3 => array(
                        'view' => 'DCCoreBundle:Pub/Partials/DTPub:actions.html.twig',
                    ),
                    4 => array(
                        'view' => 'DCCoreBundle:Pub/Partials/DTPub:delete.html.twig',
                    )
                )
            )
            ->setOrder("p.created", "desc")       ;                          // it's also possible to set the default order

//        ->setHasAction(false);
    }
    /**
     * Grid action
     */
    public function gridWaldoAction()
    {
        return $this->datatable()->execute();                                      // call the "execute" method in your grid action
    }

    /**
     * Lists all entities.
     */
    public function listSiteWaldoAction()
    {
        $this->datatable();                                                         // call the datatable config initializer
        return $this->render('DCCoreBundle:Pub:listPubDatatableWaldo.html.twig');                 // replace "XXXMyBundle:Module:index.html.twig" by yours
    }




    /*************************************************************
     * PARTIE GESTION DES PUBS MAIS DU COTE DES CATEGORIES
     *************************************************************/

    /**
     * Afficher l'interface pour attribuer une pub à une catégorie (AJAX)
     * @Template
     */
    public function attribuerPubInterfaceAction(Taxonomy $category) {
        //$pubs = $this->getDoctrine()->getRepository("DCCoreBundle:Pub")->findAll();
        $pubsByCat = $this->getDoctrine()->getRepository('DCCoreBundle:PubDetails')->getPubsDetailsByCat($category);
        return ['category'=>$category,'pubsByCat'=>$pubsByCat];
    }

    public function attribuerNouvellePubAjaxAction(Taxonomy $category){
        $pubs = $this->getDoctrine()->getRepository('DCCoreBundle:Pub')->findAll();
        $pubsByCat = $this->getDoctrine()->getRepository('DCCoreBundle:PubDetails')->getPubsDetailsByCat($category);

        // On dédoublonne, on ne veut pas des pubs qui sont déjà dans les pubsDetails de la catégorie.
        // Récuparation des ids des pubs de pubdetails de la cat
        $tab_id_pub_details = [];
        foreach($pubsByCat as $pubD) {
            $tab_id_pub_details[] = $pubD->getPub()->getId();
        }

        $pubs_a_afficher = [];
        foreach($pubs as $pub) {
            if(in_array($pub->getId(),$tab_id_pub_details) == false) {
                $pubs_a_afficher[] = $pub;
            }
        }
        $pubs = $pubs_a_afficher;

        //$pubs = $this->getDoctrine()->getRepository("DCCoreBundle:Pub")->getPubs($category);
        $datas = ['pubs'=>$pubs,'category'=>$category];
        return $this->render("DCCoreBundle:Pub/Ajax:attribuerNouvellePub.html.twig",$datas);
    }

    public function gestionPubAjaxAction(Request $request){
        $id_cat = $request->get('id_cat');
        $category = $this->getDoctrine()->getRepository("DCCoreBundle:Taxonomy")->find($id_cat);
        $pubsByCat = $this->getDoctrine()->getRepository('DCCoreBundle:PubDetails')->getPubsDetailsByCat($category);
        $datas= ['category'=>$category,'pubsByCat'=>$pubsByCat];
        return $this->render("DCCoreBundle:Pub/Ajax:gestionPub.html.twig",$datas);
    }

    /**
     * Attribuer une pub à une catégorie (AJAX)
     */
    public function attribuerPubValidationAction(Request $request) {

        // IL FAUT:
        // 1 template pour attribuer la pub avec un form Ajax à la mano. Dans ce form, on affiche toutes les pubs dispos.
        // La validation de ce form va créer une nouvelle instance de pubDetails: pub liée, is_online, url_destination
        // Lorsque qu'on clique sur la vignette de la pub, on rempli le champ caché "id_pub" avec l'id de la pub cliquée
        // 4 params à récupérer id_pub, is_online,url_destination,id_cat

        $id_pub = $request->get('id_pub');
        $is_online= $request->get('is_online');
        if($is_online == "oui") {
            $is_online = true;
        } else {
            $is_online = false;
        }


        $url_destination = $request->get('url_destination');
        $id_cat = $request->get('id_cat');

        $category = $this->getDoctrine()->getRepository("DCCoreBundle:Taxonomy")->find($id_cat);
        $pub = $this->getDoctrine()->getRepository("DCCoreBundle:Pub")->find($id_pub);
        // Créa de l'instance PubDetails
        $pubDetails = new PubDetails();
        $pubDetails->setCategory($category);
        $pubDetails->setPub($pub);
        $pubDetails->setIsOnline( $is_online);
        $pubDetails->setUrlDestination($url_destination);

        $em = $this->getDoctrine()->getManager();
        $em->persist($pubDetails);
        $em->flush();

        //$response = new Response("La publicité est attribuée",200, array('content-type' => 'text/html'));
        return $this->redirect($this->generateUrl("dc_admin_manage_taxonomy"));

    }

    // AJAX
    public function switchOnlineAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id_pub_details = $request->get('id_pub_details');
        $is_online = $request->get('is_online');
        // On inverse les valeurs car on veut switcher ce qui est demandé. Si c'était online > ça devient offline et inversement
        if($is_online == "oui") {
            $is_online = false;
            $reponse = "hors ligne";
        } else {
            $is_online = true;
            $reponse = "en ligne";
        }
        $pubDetails = $this->getDoctrine()->getRepository('DCCoreBundle:PubDetails')->find($id_pub_details);
        $pubDetails->setIsOnline($is_online);
        $em->persist($pubDetails);
        $em->flush();
        $response = new Response("La publicité est $reponse",200, array('content-type' => 'text/html'));
        return $response;
    }

    public function removePubCategoryAction(PubDetails $pubDetails) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($pubDetails);
        $em->flush();
        $response = new Response("La publicité est attribuée",200, array('content-type' => 'text/html'));
        return $response;
    }

    public function saveUrlDestinationAction(Request $request) {
        $url = $request->get('url');
        $em = $this->getDoctrine()->getManager();
        $id_pub_details = $request->get('id_pub_details');
        $pubDetails = $this->getDoctrine()->getRepository('DCCoreBundle:PubDetails')->find($id_pub_details);
        $pubDetails->setUrlDestination($url);
        $em->persist($pubDetails);
        $em->flush();
        $response = new Response("L'url est sauvegardée",200, array('content-type' => 'text/html'));
        return $response;
    }



}