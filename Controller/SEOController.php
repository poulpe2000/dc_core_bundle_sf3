<?php

namespace DC\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SEOController extends Controller
{

    /**
     * Génère le sitemap des kits tricots
     */
    public function sitemapAction()
    {
        return $this->render(
            'DCCoreBundle:SEO:sitemap.xml.twig',
            ['urls' => $this->get('dc_seo.sitemap')->makeSitemapCategory()]
        );
    }
}