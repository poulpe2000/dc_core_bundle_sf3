<?php
namespace DC\CoreBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use DC\CoreBundle\Entity\Row;
use DC\CoreBundle\Entity\Taxonomy;
use DC\CoreBundle\Entity\SiteRow;
class RowController extends Controller {


    /*****************************************************************************************************************************************
     * Gérer les lignes
     *****************************************************************************************************************************************/
    /**
     * @Template
     * page d'accueil gérer les lignes
     */
    public function manageRowsAction() {
        // Manager
        $em = $this->getDoctrine()->getManager();

        // Récupérer les catégories principales
        $main_cats = $em->getRepository("DCCoreBundle:Taxonomy")->getCategoryPrincipale();
        $gabarits = $em->getRepository("DCCoreBundle:Gabarit")->findAll();
        return array('main_cats'=>$main_cats,'gabarits'=>$gabarits);
    }

    // Afficher les lignes d'une catégorie (AJAX )
    public function loadRowsByCatAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id_cat = $request->get('id_cat');
        $cat = $em->getRepository("DCCoreBundle:Taxonomy")->find($id_cat);
        $rows = $em->getRepository('DCCoreBundle:Row')->getRowsCat($cat,false);
        return $this->render('DCCoreBundle:Row/Ajax:loadRowsByCat.html.twig',array('rows'=>$rows,'cat'=>$cat));
    }

    //  Afficher les sites de chaque ligne => permet d'optimiser le DQL et de faire moins de requêtes car les jointures sont faites dans ->getSitesInThisRow($row)
    public function loadSiteRowsAction(Request $request) {
        $id_row = $request->get('id_row');
        $em = $this->getDoctrine()->getManager();
        $row =$em->getRepository('DCCoreBundle:Row')->findOneBy(array('id'=>$id_row));
        $sitesRows = $em->getRepository('DCCoreBundle:SiteRow')->getSitesInThisRow($row);
        return $this->render('DCCoreBundle:Row/Partials:sitesRows.html.twig',array('sitesRows'=>$sitesRows));
    }


    // Sauvegarder l'ordre vertical des lignes
    public function saveOrderRowsAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        //on récupère les id, que l'on place dans un tableau pour un parcours plus aisé
        $elements = explode(",",$request->get('elements'));


        //on parcours les id, on récupère l'item correspondant, et on met son rang à jour
        foreach($elements as $key=>$element_id)
        {
            $row = $em->getRepository('DCCoreBundle:Row')->find($element_id);
            $row->setRank($key+1);
            $em->persist($row);
            $em->flush();
        }

        $text = "L'ordre des lignes a été sauvegardé";
        $reponse = new Response($text,200, array('content-type' => 'text/html'));
        return $reponse;
    }


    /*****************************************************************************************************************************************
     * Ajouter un site => gestion des lignes
     *****************************************************************************************************************************************/
    // Afficher les gabarits (Ajax)
    public function displayGabaritsAction() {
        $em = $this->getDoctrine()->getManager();
        $gabarits = $em->getRepository('DCCoreBundle:Gabarit')->findAll();
        return $this->render('DCCoreBundle:Row/Ajax:loadGabarit.html.twig',array('gabarits'=>$gabarits));
    }

    // Prévisualiser une ligne (AJAX) avant qu'elle ne soit créé dans la DB
    public function previewRowAction(Request $request) {
        $id_gabarit= $request->get('id_gabarit');
        $id_site= $request->get('id_site');
        $em = $this->getDoctrine()->getManager();
        $gabarit = $em->getRepository('DCCoreBundle:Gabarit')->find($id_gabarit);
        $site = $em->getRepository('DCCoreBundle:Site')->find($id_site);
        $cols = explode('-',$gabarit->getName());
        $colors = $em->getRepository('DCCoreBundle:Color')->findAll();
//        $main_pic = $em->getRepository('DCCoreBundle:Pics')->getMainPicSite($site);

        return $this->render('DCCoreBundle:Row/Ajax:loadNewRowPreview.html.twig',array('cols'=>$cols,'site'=>$site,'gabarit'=>$gabarit,'colors'=>$colors,'main_pic'=>$site->getMainPic()));
    }
    //Afficher la galerie de photos du site pour en choisir une et l'associer à une vignette
    public function showGalleryAction(Request $request) {
        $id_site = $request->get('id_site');
        // Permet de savoir si on est sur la page addRow ou addThumb
        $small_thumbs = $request->get('small_thumbs');
        $em =$this->getDoctrine()->getManager();
        $site = $em->getRepository('DCCoreBundle:Site')->find($id_site);
        $pics = $em->getRepository('DCCoreBundle:Pics')->getPicsSite($id_site);
        return $this->render('DCCoreBundle:Row/Ajax:showGallery.html.twig',array('site'=>$site,'pics'=>$pics,'small_thumbs'=>$small_thumbs));
    }

    // Ajouter une ligne dans la DB
    private function makeRow($category,$gabarit,$rank =1) {

        $row = new Row();
        $row->setIsComplet(false);
        $row->setCategory($category);
        $row->setGabarit($gabarit);
        $row->setIsOnline(false);
        $row->setRank($rank);
        return $row;

    }
    // Associer un sites à une ligne dans la DB
    private function makeSiteRow($row,$nbCols,$site,$color,$position = 0) {
        $site_row = new SiteRow();
        $site_row->setIsBolobolo(false);
        $site_row->setRow($row);
        $site_row->setSite($site);
        $site_row->setColor($color);
        $site_row->setNbCols($nbCols);
        $site_row->setPosition($position);


        return $site_row;
    }

    /********************************************************
     * NEW 17/05/2017 >> supprimer un site d'une row AJAX
     *******************************************************/
    public function removeSiteRowAction(Request $request) {
        $id_site_row = $request->get('id_site_row');
        $id_row = $request->get('id_row');
        $em = $this->getDoctrine()->getManager();
        $site_row = $this->getDoctrine()->getRepository("DCCoreBundle:SiteRow")->find($id_site_row);
        $row = $this->getDoctrine()->getRepository("DCCoreBundle:Row")->find($id_row);
        $row->setIsComplet(false);
        $em->persist($row);
        $em->remove($site_row);
        $em->flush();
        $text = "La site a été retiré. ";
        $reponse = new Response($text,200, array('content-type' => 'text/html'));
        return $reponse;
    }


    /*****************************************************************************************************************************************
     * Ajouter un site => Créer une nouvelle ligne
     *****************************************************************************************************************************************/
    // Sauvegarder une nouvelle ligne avec au début 1 seul site (forcément)
    public function saveNewLigneAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id_category= $request->get('id_cat');
        $id_gabarit= $request->get('id_gabarit');
        $id_site= $request->get('id_site');
        $nbCols= $request->get('nb_cols');
        $id_color = $request->get('id_color');
        $color = $em->getRepository('DCCoreBundle:Color')->find($id_color);
        $category = $em->getRepository('DCCoreBundle:Taxonomy')->find($id_category);
        $gabarit = $em->getRepository('DCCoreBundle:Gabarit')->find($id_gabarit);
        $site = $em->getRepository('DCCoreBundle:Site')->find($id_site);
        // Réoordonner toutes les lignes pour libérer le premier rang
        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('DCCoreBundle:Row')->getRowsCat($category,false);
        foreach($rows as $r) {
            $new_rank = $r->getRank();
            $new_rank += 1;
            $r->setRank($new_rank);
            $em->persist($r);
            $em->flush();
        }
        $row = $this->makeRow($category,$gabarit);
        $em->persist($row);
        $site_row = $this->makeSiteRow($row,$nbCols,$site,$color);
        $em->persist($site_row);
        $em->flush();

        $text = "La ligne contenant le site ".$site->getDomain()." a été créé. ";
        $reponse = new Response($text,200, array('content-type' => 'text/html'));
        return $reponse;
//        $this->get('session')->getFlashBag()->add('success',"La ligne a été créée");
//        return $this->redirect($this->generateUrl('dc_admin_add_site'));
    }

    /*****************************************************************************************************************************************
     * Ajouter un site => L'ajouter à une ligne existante
     *****************************************************************************************************************************************/
    /// Récupérer une ligne en cours de création
    // 1-> Faire une req pour récupérer la dernière ligne créée d'une catégorie qui n'est pas complète.
    // 2-> Appel Ajax pour la prévisualiser. Le site en cours de créa s'ajoute automatiquement dans un emplacement libre, juste après le précédent site
    // 3-> Enregistrer la ligne => on fait appelle à l'action ajax // Enregistrer une ligne
        //$("#add_rows").on('click','.save-ligne',function(){ ...
    //  Dans l'action saveNewLigneAction, si c'est une nouvelle ligne on laisse telle quelle, sinon on ne recré pas de nouvelle ligne,
    //  on supprime toutes les lignes et on les créé à nouveau. C'est plus facile pour les mises à jours.
    // 4->Si ligne pas terminée on renvoi vers le form pour ajouter un nouveau site.
    // ou bien si ligne terminée, on passe à l'étape finalisation > action finalierRowAction , dans laquelle on pourra publier la ligne et
    // reordonner l'emplacement des sites au sein de la ligne.

    // Afficher les lignes existantes
    public function loadRowsAction(Taxonomy $category) {
        $em = $this->getDoctrine()->getManager();
        $rows = $em->getRepository('DCCoreBundle:Row')->getRowsCat($category,false);
        return $this->render('DCCoreBundle:Row/Partials:loadRows.html.twig',array('rows'=>$rows));
    }

    // Ajouter un site à une ligne existante
    public function addSiteToRowAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id_row = $request->get('id_row');
        $id_site = $request->get('id_site');
        $row = $em->getRepository('DCCoreBundle:Row')->findOneBy(array('id'=>$id_row));
        $new_site = $em->getRepository('DCCoreBundle:Site')->findOneBy(array('id'=>$id_site));
        // Différence entre nbre de site attendus dans le gabarit et nbre de site déjà créé pour la ligne
        // Le reste servira à afficher des vignettes bolobolo
        // 1 --> on récupère le nombre sites associés à la row
        $site_rows_total = $row->getSiteRowsCount();
        //2 --> on compte le nombre de sites attendus dans le gabarit
        $gabarit = explode('-',$row->getGabarit()->getName());
        $nb_site_gabarit = count($gabarit);
        // 3 --> déduction du nbre de bolo
        $nb_bolobolo = $nb_site_gabarit - $site_rows_total;
        // 4 --> Ensuite on déduit du bolobolo, le site qu'on est en train d'ajouter
        $nb_bolobolo -=1;
        // Emplacement du nouveau site à ajouter
        // ==>>Calculer le nombre de colonne qu'il faut pour ce nouveau site. Fonction de son emplacement et du gabarit
        // 1--> Calcul de son emplacement
        $emplacement_site = $site_rows_total+1;
        // On regarde par rapport aux chiffres des cols stockés dans le tableau $gabarit. Comme un tableau à 0 , on enlève 1
        $nb_col = $gabarit[$emplacement_site-1];
        // Ensuite on donne le nombre de cols aux bolobolos restants;
        // On récupère la dernière portion du tableau $gabarit => Donc pas le premier emplacement et pas le deuxième car c'est le site nouveau ajouté
        $gabarit_bolobolo = array_slice($gabarit,$emplacement_site);
        // Pour chaque on stocke le nbre de col dans le tableau $bolobolos
        $bolobolos = array();
        foreach($gabarit_bolobolo as $gb) {
            $bolobolos[] = $gb;
        }
        $colors = $em->getRepository('DCCoreBundle:Color')->findAll();
        return $this->render('DCCoreBundle:Row/Ajax:loadAddSiteRow.html.twig',array('row'=>$row,'nb_bolobolo'=>$nb_bolobolo,'colors'=>$colors,'new_site'=>$new_site,'nb_col'=>$nb_col,'bolobolos'=>$bolobolos));
    }

    // Modifier une ligne existante (manageRows)
    public function editRowAction(Request $request){
        $id_row = $request->get('id_row');
        $em = $this->getDoctrine()->getManager();
        $row = $em->getRepository("DCCoreBundle:Row")->find($id_row);
        $sitesRow = $em->getRepository('DCCoreBundle:SiteRow')->getSitesInThisRow($id_row);
        $bolobolos = array();
        if($row->getIsComplet()== false) {
            // DEDUCTION DU NBRE DE BOLOBOLOS
            // 1 --> on récupère le nombre sites associés à la row
            $site_rows_total = $row->getSiteRowsCount();
            //2 --> on transforme la chaine name gabarit en tableau 4-4-4 => array(4,4,4)
            $gabarit = explode('-',$row->getGabarit()->getName());
            // Ensuite on donne le nombre de cols aux bolobolos restants;
            // On récupère la dernière portion du tableau $gabarit
            $gabarit_bolobolo = array_slice($gabarit,$site_rows_total);
            // Pour chaque on stocke le nbre de col dans le tableau $bolobolos
            foreach($gabarit_bolobolo as $gb) {
                $bolobolos[] = $gb;
            }
        }
        $colors = $em->getRepository('DCCoreBundle:Color')->findAll();
        $datas = array('row'=>$row,'bolobolos'=>$bolobolos,'colors'=>$colors,'sitesRow'=>$sitesRow);
        return $this->render('DCCoreBundle:Row/Ajax:loadEditRow.html.twig',$datas);
    }


    // Supprimer tous les sites associés à une ligne SiteRows models
    public function deleteSitesRowAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id_row = $request->get('id_row');

        $rows = $em->getRepository('DCCoreBundle:SiteRow')->getSitesInThisRow($id_row);
        foreach($rows as $r ) {
            $em->remove($r);
            $em->flush();
        }
        $text = "Les sites de la lignes ont été supprimés. ";
        $reponse = new Response($text,200, array('content-type' => 'text/html'));
        return $reponse;
    }
    // Sauvegarder le site qui vient d'être ajouté à la row existante
    public function saveNewSiteRowAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        // C'est bien sauf qu'il faut qu'il faut supprimer tous les sites dans siteRows pour cette ligne et les resauvegarder. C'est plus facile pour prendre en compte les modifs
        // A priori je récupère par la couleur de tous les sites pour l'instant => voir d'ou vien le bug c'est pour le premier site qu'on l'a pas
        $id_row = $request->get('id_row');
        $row = $em->getRepository('DCCoreBundle:Row')->findOneBy(array('id'=>$id_row));

        $id_site = $request->get('id_site');
        $id_color = $request->get('id_color');
        $nbCols= $request->get('nb_cols');
        $position = $request->get('position');


        $site = $em->getRepository('DCCoreBundle:Site')->findOneBy(array('id'=>$id_site));

        $color = $em->getRepository('DCCoreBundle:Color')->find($id_color);
        $siteRow = $this->makeSiteRow($row,$nbCols,$site,$color,$position);
        $em->persist($siteRow);
        $em->flush();

        // Verifier si la ligne est complète=> si oui on la met à isComplet(true)
        // 1 --> on récupère le nombre sites associés à la row
        $site_rows_total = $row->getSiteRowsCount();
        //2 --> on compte le nombre de sites attendus dans le gabarit
        $gabarit = explode('-',$row->getGabarit()->getName());
        $nb_site_gabarit = count($gabarit);
        if($site_rows_total == $nb_site_gabarit) {
            $row->setIsComplet(true);
            $em->persist($row);
            $em->flush();
        }

        $text = "Le site ".$site->getDomain()." a été ajouté à la ligne. ";
        $reponse = new Response($text,200, array('content-type' => 'text/html'));
        return $reponse;
    }
    // Sauvegarder l'ordre des sitesRows
    public function saveOrderSiteRowAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $elements = explode(",",$request->get('elements'));
        //on parcours les id, on récupère l'item correspondant, et on met son rang à jour
        foreach($elements as $key=>$element_id)
        {
            var_dump($elements);
//            $row_count = $em->getRepository('DCCoreBundle:SiteRow')->findOneBy(array('id'=>$element_id));
//            var_dump($row_count);
//            if (count($row_count) ==1) {
                $row = $em->getRepository('DCCoreBundle:SiteRow')->find($element_id);

                $row->setPosition($key+1);
                $em->persist($row);
                $em->flush();
//            }
        }
        $text = "L'ordre des sites dans la ligne a été sauvegardé";
        $reponse = new Response($text,200, array('content-type' => 'text/html'));
        return $reponse;
    }


    // Publier une ligne
    public function publishRowAction(Row $row) {
        $em = $this->getDoctrine()->getManager();
        $row->setIsOnline(true);
        $em->persist($row);
        $em->flush();
        $request = $this->get('request');
        if($request->isXmlHttpRequest()){
            $text = "La ligne est publiée. ";
            $reponse = new Response($text,200, array('content-type' => 'text/html'));
            return $reponse;
        } else {
            return $this->redirect($this->generateUrl('dc_admin_add_site'));
        }
    }

    // Mettre offline une ligne
    public function offlineRowAction(Row $row) {
        $em = $this->getDoctrine()->getManager();
        $row->setIsOnline(false);
        $em->persist($row);
        $em->flush();
        $request = $this->get('request');
        if($request->isXmlHttpRequest()){
            $text = "La ligne est offline. ";
            $reponse = new Response($text,200, array('content-type' => 'text/html'));
            return $reponse;
        } else {
            return $this->redirect($this->generateUrl('dc_admin_add_site'));
        }
    }

    public function checkIfRowIsOnlineAction(Row $row) {
        if($row->getIsOnline()==true){
            $isOnline = "true";
        } else {
            $isOnline ="false";
        }
        $reponse = new Response($isOnline,200, array('content-type' => 'text/html'));
        return $reponse;
    }



// DATATABLES, afficher les lignes existantes dans la partie Ajouter un site à une ligne existante



    /**
     * Grid action
     * @return Response
     */
    public function gridAction(Request $request)
    {
        $id_cat = $request->get('id');
        $datatable = $this->get('dc_datatables.row');
        return $datatable->makeDatatable($id_cat)->execute();                                      // call the "execute" method in your grid action
    }


    /**
     * Lists all entities.
     * @return Response
     */
    public function RowsDTAction(Request $request)
    {
        $id_cat = $request->get('id_cat');
        $datatable = $this->get('dc_datatables.row');
        $datatable->makeDatatable($id_cat);                                                        // call the datatable config initializer
        return $this->render('DCCoreBundle:Row/Ajax:rowsListAjaxDT.html.twig',array('id'=>$id_cat));                 // replace "XXXMyBundle:Module:index.html.twig" by yours
    }




}