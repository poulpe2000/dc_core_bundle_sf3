<?php
namespace DC\CoreBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use DC\CoreBundle\Entity\Taxonomy;
use DC\CoreBundle\Form\TaxonomyType;
use DC\CoreBundle\Form\FormHandler;

class TaxonomyController extends Controller {
    /**
     * @Template
     */
    public function manageTaxonomyAction() {
        $em = $this->getDoctrine()->getManager();
        $cats = $em->getRepository('DCCoreBundle:Taxonomy')->getRootNodes();


        return array('cats'=>$cats);
    }

    public function manageTagsAction() {
        $em = $this->getDoctrine()->getManager();
        $tags = $em->getRepository('DCCoreBundle:Taxonomy')->findTags();
        return $this->render('DCCoreBundle:Taxonomy:manageTags.html.twig',['tags'=>$tags]);
    }
    public function manageMarquesAction() {
        $em = $this->getDoctrine()->getManager();
        $marques = $em->getRepository('DCCoreBundle:Taxonomy')->findMarques();
        return $this->render('DCCoreBundle:Taxonomy:manageMarques.html.twig',['marques'=>$marques]);
    }

    public function manageTaxonomyV2Action() {
        $roots = $this->getDoctrine()->getRepository("DCCoreBundle:Taxonomy")->findAll();
        $repo = $this->getDoctrine()->getRepository("DCCoreBundle:Taxonomy");
//        $options = array(
//            'decorate' => true,
//            'rootOpen' => '<ul>',
//            'rootClose' => '</ul>',
//            'childOpen' => '<li>',
//            'childClose' => '</li>',
//            'nodeDecorator' => function($node) {
//                return $node['nom'];
//            }
//        );
//        $htmlTree = $repo->childrenHierarchy(
//            null, /* starting from root nodes */
//            false, /* false: load all children, true: only direct */
//            $options
//        );



        return $this->render('@DCCore/Taxonomy/manageTaxonomyV2.html.twig', array('roots' => $roots,'repo'=>$repo));
    }

    private function parcourir($roots ) {
        $arbre = [];

            foreach($roots as $root) {
                if($root->hasParent()===false) {
                    $arbre[]['parent'] = $root->getNom();

                }

                if(count($root->getChildren())>0) {
                   array_push($arbre,$root->getChildren());
                }
        }
        return $arbre;
}

    public function buildTree($roots) {

        $tree = array('cat_parent'=>'laine',
                      'cats_enfants'=> array(
                                      'level-0'=>array('laine pas cher','katia'),
                                      'level-1'=>array('laine pas cher','katia')
                      ));
    }


    /**
     * @Template
     * Ajouter une catégorie
     */
    public function addTaxonomyAction(Request $request) {
        $cat = new Taxonomy();
        $form = $this->createForm( TaxonomyType::class,$cat);
        $em= $this->getDoctrine()->getManager();
        $formHandler = new FormHandler($form,$request,$em);
        if ($formHandler->process()) {
            if($form->getData()->getIsActive() == 0) {
                $redirect = 0;
                $cat->setIsActive(true);
                $cat->setIsMarque(false);
                $cat->setIsTag(false);
            } else if ($form->getData()->getIsActive()== 1) {
                $redirect = 1;
                $cat->setIsActive(false);
                $cat->setIsMarque(false);
                $cat->setIsTag(true);
            }else if ($form->getData()->getIsActive()==2) {
                $redirect = 2;
                $cat->setIsActive(false);
                $cat->setIsMarque(true);
                $cat->setIsTag(false);
            }
            else if ($form->getData()->getIsActive()==3) {
                $redirect = 0;
                $cat->setIsActive(false);
                $cat->setIsMarque(false);
                $cat->setIsTag(false);
            }
            $em->persist($cat);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success',"Catégorie ajoutée");
            if($redirect ==0) {
                return $this->redirect($this->generateUrl('dc_admin_manage_taxonomy'));
            } else if($redirect ==1) {
                return $this->redirect($this->generateUrl('dc_admin_manage_tags'));
            } else if($redirect ==2) {
                return $this->redirect($this->generateUrl('dc_admin_manage_marques'));
            }
        }
        return array('form'=>$form->createView());
    }

    // Supprimer une catégorie
    public function delCatAction(Taxonomy $cat) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($cat);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',"La catégorie ".$cat->getNom()." a été supprimée");
        return $this->redirect($this->generateUrl('dc_admin_manage_taxonomy'));
    }

    // Activer une catégorie
    public function activeCatAction(Taxonomy $cat) {
        $cat->setIsActive(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($cat);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',"La catégorie est active");
        return $this->redirect($this->generateUrl('dc_admin_manage_taxonomy'));

    }
    // Désactiver une catégorie
    public function deactiveCatAction(Taxonomy $cat) {
        $cat->setIsActive(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($cat);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',"La catégorie est désactivée");
        return $this->redirect($this->generateUrl('dc_admin_manage_taxonomy'));

    }

    // Compter le nombre de site dans une catégorie
    public function countSiteInCatAction(Taxonomy $cat) {
        $em = $this->getDoctrine()->getManager();
        $sites_in_this_cat = $em->getRepository('DCCoreBundle:Site')->getSitesInThisCat($cat);
        $total = count($sites_in_this_cat);
        if($total>0)
        {
            $response = new Response("<span class='label label-success'>".$total."</span>", 200, array('content-type' => 'text/html'));

        } else {
            $response = new Response("<span class='label label-default'>".$total."</span>", 200, array('content-type' => 'text/html'));
        }
        return $response;
    }

    public function editTaxonomyAction(Taxonomy $t, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(TaxonomyType::class,$t);
        $formHandler = new FormHandler($form,$request,$em);
        if ($formHandler->process()) {
            if($form->getData()->getIsActive() == 0) {
                $redirect = 0;
                $t->setIsActive(true);
                $t->setIsMarque(false);
                $t->setIsTag(false);
            } else if ($form->getData()->getIsActive()== 1) {
                $redirect = 1;
                $t->setIsActive(false);
                $t->setIsMarque(false);
                $t->setIsTag(true);
            }else if ($form->getData()->getIsActive()==2) {
                $redirect = 2;
                $t->setIsActive(false);
                $t->setIsMarque(true);
                $t->setIsTag(false);
            }
            else if ($form->getData()->getIsActive()==3) {
                $redirect = 0;
                $t->setIsActive(false);
                $t->setIsMarque(false);
                $t->setIsTag(false);
            }
            $em->persist($t);
            $this->get('session')->getFlashBag()->add('success',"Catégorie modifiée");
            $t->getIco()->setUpdateRand(uniqid());
            $em->persist($t);
            $em->flush();
            if($redirect ==0) {
                return $this->redirect($this->generateUrl('dc_admin_manage_taxonomy'));
            } else if($redirect ==1) {
                return $this->redirect($this->generateUrl('dc_admin_manage_tags'));
            } else if($redirect ==2) {
                return $this->redirect($this->generateUrl('dc_admin_manage_marques'));
            }
        }
        return $this->render('DCCoreBundle:Taxonomy:addTaxonomy.html.twig',array('form'=>$form->createView()));
    }


    public function searchAction(Taxonomy $marque)
    {
        $finder = $this->get('fos_elastica.finder.app.site');
//        $searchTerm = $request->query->get('search');
//        $boolQuery = new \Elastica\Query\BoolQuery();
//        $fieldQuery = new \Elastica\Query\MultiMatch();
//        $fieldQuery->setFields(array('ancre'));
//        $fieldQuery->setType('most_fields');
//        $fieldQuery->setMaxExpansions(100);
//        $fieldQuery->setQuery('Katia',100);

        $sites = $finder->find($marque->getNom(),1000);
        return $this->render('DCCoreBundle:Taxonomy:search.html.twig',array('sites'=>$sites,'marque'=>$marque));
    }

    public function activateMarqueAction(Taxonomy $marque,Request $request) {
        $marque->setIsMarqueOnline(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($marque);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',"Marque affichée");
          return $this->redirect($this->generateUrl('dc_admin_manage_marques'));
    }

    public function deactivateMarqueAction(Taxonomy $marque,Request $request) {
        $marque->setIsMarqueOnline(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($marque);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success',"Marque non affichée");
          return $this->redirect($this->generateUrl('dc_admin_manage_marques'));
    }

}