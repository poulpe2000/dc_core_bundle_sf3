<?php
/**
 * Created by PhpStorm.
 * User: poulpe
 * Date: 13/02/15
 * Time: 14:49
 */

namespace DC\CoreBundle\Controller;
use DC\CoreBundle\Datatables\SiteDatatable;
use DC\CoreBundle\Form\PicsType;
use DC\CoreBundle\Form\SiteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use DC\CoreBundle\Entity\Site;
use DC\CoreBundle\Entity\Pics;
use DC\CoreBundle\Entity\Row;
use DC\CoreBundle\Form\FormHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SiteController extends Controller{
    // Ajouter un site
//    /**
//     * @Template
//     */
//    public function addSiteAction(Request $request) {
//        $site = new Site;
//        $form = $this->createForm(new SiteType(),$site);
//        $em = $this->getDoctrine()->getManager();
//        if($request->getMethod() == 'POST') {
//            $form->handleRequest($request);
//            if($form->isValid()) {
////                $domain = parse_url($site->getUrl());
////                $site->setDomain(ltrim($domain['host'],'www.'));
//                $em->persist($site);
//                $em->flush();
//                $json = json_encode($site);
//                return new Response($json) ;
//            }
//        }


//        return array('form'=>$form->createView());
//
//    }
    /***********************************************
     * ETAPE 1 ==> Formulaire pour Ajouter un site
     **********************************************/
    // Autocomplete // Rechercher doublon site avant ajout

    public function searchUrlAction(Request $request) {
        $em =$this->getDoctrine()->getManager();
        if($request->isXmlHttpRequest())
        {
            $term = $request->get('url');
            $sites = $em->getRepository('DCCoreBundle:Site')->findLikeUrl($term);
            $array_urls = array();
            foreach($sites as $site)
            {
//                echo $site->getUrl();
//                var_dump($sites);
                $array_urls[] = array("url"=>$site->getUrl());
            }
            $response = new Response(json_encode($array_urls));
            $response -> headers -> set('Content-Type', 'application/json');
            return $response;
        }

    }

    /**
     * @Template
     */
    public function addSiteAction(Request $request) {
        $site = new Site();
        $form = $this->createForm( SiteType::class,$site);
        $em = $this->getDoctrine()->getManager();
        $formHanlder = new FormHandler($form,$request,$em);
        if($formHanlder->process()) {
            $domain = parse_url($site->getUrl());
            $site->setDomain(ltrim($domain['host'],'www.'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($site);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success',"site ajoutée");
            return $this->redirect($this->generateUrl('dc_admin_add_site_thumb',array('id'=>$site->getId())));
        }
        return array('form'=>$form->createView());
    }

    // Modifier un site
    public function editSiteAction(Site $site,Request $request){
        $form = $this->createForm(SiteType::class,$site);
        $em = $this->getDoctrine()->getManager();
        $formHanlder = new FormHandler($form,$request,$em);

        if($formHanlder->process()) {
            if($site->getIsOffline()!=true) {
                $domain = parse_url($site->getUrl());
                $site->setDomain(ltrim($domain['host'],'www.'));
                $em = $this->getDoctrine()->getManager();
                $em->persist($site);
                $em->flush();
            }

            $response = new Response("Le site est modifié",200, array('content-type' => 'text/html'));
            return $response;
        }
        return $this->render('DCCoreBundle:Site/Ajax:editSite.html.twig',array('site'=>$site,'form'=>$form->createView()));

    }


    // Si changement de tags, on change le choix des cats principales associées
    public function tagsListenerAction(Request $request){
        $ids_tags= $request->get('ids_tags');
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository('DCCoreBundle:Taxonomy')->getCatsWithThisIds($ids_tags);
        return $this->render('DCCoreBundle:Site/Ajax:cats_form.html.twig', array('categories'=> $categories));

    }

    // Extraire les balises meta (AJAX)
    public function extractMetaTagsAction(Request $request) {
        $url = $request->get('url');
        $metas = get_meta_tags($url);
         if(strlen($metas['description']) <1) {
            $text = "Il n'a pas de balise meta description";
        } else {
            $text = $metas['description'];
        }
        $response = new Response($text,200, array('content-type' => 'text/html'));
        return $response;
    }

    /***********************************************
     * ETAPE 2 ==> Générer la vignette
     **********************************************/
    /**
     *
     * @Template
     */
    public function addSiteThumbAction(Site $site, Request $request) {
        $pic = new Pics();
        $pic->setSite($site);
        $pic->setIsCropped(false);
        $pic->setIsMain(false);
        $form_upload = $this->createForm(PicsType::class,$pic);
        $em = $this->getDoctrine()->getManager();
        $formHandler = new FormHandler($form_upload,$request,$em);
        if ($request->isXmlHttpRequest()) {


        if($formHandler->process()) {
            $site->setMainPic($pic);
            $em->persist($site);
            $em->flush();

//            return $this->redirect($this->generateUrl('dc_admin_add_site_thumb',array('id'=>$site->getId())));
            return $this->render('DCCoreBundle:Site/Ajax:thumb.html.twig',array('pic'=>$pic));
        }
        }
        return array('site'=>$site,'form'=>$form_upload->createView());
    }


    // Générer une vignette (AJAX)
    public function doThumbAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $url = $request->get('url');
        $site_id = $request->get('id_site');
        $site = $em->getRepository('DCCoreBundle:Site')->findOneBy(array('id' => $site_id));

        $thumb_path_uniq_name = sha1(uniqid(mt_rand(), true)).".jpg";
//        $path_thumb = "/var/www/vhosts/laine-et-maille.com/httpdocs/shared/web/uploads/thumbs/".$thumb_path_uniq_name;
        $path_thumb =$request->server->get('DOCUMENT_ROOT')."/uploads/thumbs/".$thumb_path_uniq_name;


        $pic= new Pics();
        $pic->setPath($thumb_path_uniq_name);
        $pic->setIsMain(false);
        $pic->setSite($site);
        $pic->setFile("screen");
        $pic->setIsCropped(false);

        $em->persist($pic);

        $site->setMainPic($pic);

        $em->persist($site);

        $em->flush();

       $this->get('knp_snappy.image')->generate($url,$path_thumb);




        return $this->render('DCCoreBundle:Site/Ajax:thumb.html.twig',array('pic'=>$pic));
    }

    // Récupérer la dernière vignette créée (AJAX
    public function displayLastPicAction($id_site,$row_preview = null) {
        $em = $this->getDoctrine()->getManager();
        $last_pic = $em->getRepository('DCCoreBundle:Pics')->getLastPic($id_site);
//        var_dump($last_pic);
        return $this->render('DCCoreBundle:Site/Ajax:thumb.html.twig',array('pic'=>$last_pic,'row_preview'=>$row_preview));

    }

    // Crop Image (AJAX)
    public function makeCropImageAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $site_id = $request->get('site_id');
        $data = $request->get('img_data');
        $file = 'crop_'.uniqid().'bim.png';
        $site = $em->getRepository('DCCoreBundle:Site')->findOneBy(array('id' => $site_id));
        $path_crop_thumb =$request->server->get('DOCUMENT_ROOT')."/uploads/thumbs/".$file;
//
        $image = explode('base64,',$data);
        file_put_contents($path_crop_thumb, base64_decode($image[1]));

        $pic= new Pics();
        $pic->setPath($file);
        $pic->setIsMain(false);
        $pic->setSite($site);
        $pic->setIsCropped(true);
        $pic->setFile("crop");
        $em->persist($pic);
        $em->flush();
//        return $this->render('DCCoreBundle:Site/Ajax:responseCrop.html.twig',array('site_id'=>$site_id));
        $response = new Response("La vignette recadrée est sauvegardée",200, array('content-type' => 'text/html'));
        return $response;
    }
    // Rafraichier le bouton newLigne (Ajax)

    public function refreshBtnNewLigneAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id_site = $request->get('id_site');
        $site = $em->getRepository('DCCoreBundle:Site')->findOneBy(array('id'=>$id_site));
        return $this->render('DCCoreBundle:Site/Ajax:loadNewLigneBtn.html.twig',array('site'=>$site));
    }


    // Changer l'image principale d'un site (AJAX)
    public function changeMainPicAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $id_pic = $request->get('id_pic');
        $id_site = $request->get('id_site');
        $thumb_gallery_p = $request->get('thumb_gallery_p');
        $site = $em->getRepository('DCCoreBundle:Site')->findOneBy(array('id'=>$id_site));
        $pic = $em->getRepository('DCCoreBundle:Pics')->findOneBy(array('id' => $id_pic));
//        $pics_site = $em->getRepository('DCCoreBundle:Pics')->getPicsSite($id_site);
        $site->setMainPic($pic);
        $em->persist($site);
        $em->flush();
//        foreach($pics_site as $ps) {
//            $ps->setIsMain(false);
//            $em->persist($ps);
//            $em->flush();
//        }
//        $pic->setIsMain(true);
//        $em->persist($pic);
//        $em->flush();
        if($thumb_gallery_p != 'false')  {
            return $this->render('DCCoreBundle:Site/Ajax:thumb.html.twig',array('pic'=>$site->getMainPic()));
        } else {
            return $this->render('DCCoreBundle:Site/Ajax:loadMainPic.html.twig',array('pic'=>$site->getMainPic()));
        }

    }

    public function deletePicAction(Pics $pic){
        $em = $this->getDoctrine()->getManager();
        $em->remove($pic);
        $em->flush();
        $response = new Response("La vignette a été supprimée",200, array('content-type' => 'text/html'));
        return $response;
    }

    /************************************************************************
     * ETAPE 3 ==> Ajouter le site à une ligne de sa catégorie principale
     ************************************************************************/
    // Cette catégorie princiaple à t'elle déjà une ligne en cours de création ?
    // Si oui :=> la compléter ou bien ==> chemin R1
    // Si non :=> en créer une nouvelle ==> chemin R2

    /**
     * @Template
     */
    public function addSiteRowAction(Site $site) {
        return array('site'=>$site);
    }
    // LE reste des actions se trouvent dans le controller RowController



// DATATABLES, afficher les lignes existantes dans la partie Ajouter un site à une ligne existante


/******* OLD DATATABLE >> SF 2 *********/
//    /**
//     * Grid action
//     * @return Response
//     */
//    public function gridSiteAction(Request $request)
//    {
//        $datatable = $this->get('dc_datatables.site');
//        return $datatable->makeDatatable()->execute();                                      // call the "execute" method in your grid action
//    }
//
//
//    /**
//     * Lists all entities.
//     * @return Response
//     */
//    public function SitesDTAction()
//    {
//
//        $datatable = $this->get('dc_datatables.site');
//        $datatable->makeDatatable();                                                        // call the datatable config initializer
//        return $this->render('DCCoreBundle:Site/Ajax:sitesListAjaxDT.html.twig');                 // replace "XXXMyBundle:Module:index.html.twig" by yours
//    }




    /******* NEW DATATABLE >> SF 3 WALDO WALDO *********/
public function datatable() {
    $controller_instance = $this;
    $datatable = $this->get('datatable');
    return $datatable
        ->setSearch(true)
        ->setSearchFields(array(0))
        ->setEntity('DCCoreBundle:Site','s')

        ->setFields(
            array(
                "domaine" => "s.domain",
                "Catégorie" => "c.nom",
                "Url Ancre " => "s.url",
                "vignette"=> "p.path",
                "Ancre" => "s.ancre",
                "No Follow ?" => "s.isNoFollowAncre",
                "Description" => "s.showDescShort",
                "Créé le "=> "s.created",
                "Actions"         => 's.id',
                "_identifier_"  => 's.id'
            )
        )
        ->addJoin('s.mainPic', 'p', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
        ->addJoin('s.category', 'c', \Doctrine\ORM\Query\Expr\Join::INNER_JOIN)
        ->setRenderer(
            function(&$data) use ($controller_instance)
            {
                foreach ($data as $key => $value)
                {

                    if ($key == 3)                                      // 1 => address field
                    {
                        $data[$key] =
                            $controller_instance->get('templating')
                                ->render(
                                    'DCCoreBundle:Site/Partials/DTSite:mainPic.html.twig',
                                    array('site' => $value)
                                );
                    }
                    if ($key == 5)                                      // 1 => address field
                    {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
                        $data[$key] =  $controller_instance->get('templating')
                            ->render(
                                'DCCoreBundle:Site/Partials/DTSite:noFollow.html.twig',
                                array('noFollow' => $value)
                            );
                    }
                    if ($key == 6)                                      // 1 => address field
                    {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
                        $data[$key] = $controller_instance->get('templating')
                            ->render(
                                'DCCoreBundle:Site/Partials/DTSite:showShortDesc.html.twig',
                                array('showShortDesc' => $value)
                            );
                    }
                    if ($key == 7)                                      // 1 => address field
                    {
//                            $data[$key] = $controller_instance
//                                ->get('templating')
                        $data[$key] =  $controller_instance->get('templating')
                            ->render(
                                'DCCoreBundle:Site/Partials/DTSite:date.html.twig',
                                array('created' => $value)
                            );
                    }
                }
            }
        )
        ->setRenderers(
            array(
                2 => array(
                    'view' => 'DCCoreBundle:Site/Partials/DTSite:urlAncre.html.twig',
                ),
                8 => array(
                    'view' => 'DCCoreBundle:Site/Partials/DTSite:actions.html.twig',
                )
            )
        )
        ->setOrder("s.created", "desc")       ;                          // it's also possible to set the default order

//        ->setHasAction(false);
}
    /**
     * Grid action
     */
    public function gridWaldoAction()
    {
        return $this->datatable()->execute();                                      // call the "execute" method in your grid action
    }

    /**
     * Lists all entities.
     */
    public function listSiteWaldoAction()
    {
        $this->datatable();                                                         // call the datatable config initializer
        return $this->render('DCCoreBundle:Site:listSitesDatatableWaldo.html.twig');                 // replace "XXXMyBundle:Module:index.html.twig" by yours
    }

    /******* FIN NEW DATATABLE >> SF 3 WALDO WALDO *********/

    public function manageAction(){
        return $this->render('DCCoreBundle:Site:listSite.html.twig');
    }

    public function putOfflineAction(Site $site) {
        $em = $this->getDoctrine()->getManager();
        $site->setIsOffline(true);
        $url_error = $site->getUrl();
        $site->setUrlError($url_error);
        $site->setUrl($this->generateUrl('dc_front_site_offline', array('id'=>$site->getId())));
        $em->persist($site);
        $em->flush();

        $response = new Response("Le site est offline",200, array('content-type' => 'text/html'));
        return $response;
    }



}