<?php

namespace DC\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use DC\CoreBundle\Entity\Taxonomy;
use DC\CoreBundle\Entity\Row;
use DC\CoreBundle\Entity\Site;
class FrontController extends Controller
{
    /**
     * @Template
     */
    public function indexAction() {
        $em= $this->getDoctrine()->getManager();
        $sites = $em->getRepository('DCCoreBundle:Site')->getSitesMainPic();

//        return $this->render('DCCoreBundle:Front:index.html.twig');
//        return $this->redirect($this->generateUrl('dc_front_view_by_main_cat',array('slug'=>'laine')));
//        $cats = $em->getRepository('DCCoreBundle:Taxonomy')->getCategoryPrincipale("id");
        $cats = $em->getRepository('DCCoreBundle:Taxonomy')->getRootNodes("id","asc");
        return array('cats'=>$cats,'sites'=>$sites);
    }



    public function getMainCategoriesAction() {
        $em= $this->getDoctrine()->getManager();
//        $cats = $em->getRepository('DCCoreBundle:Taxonomy')->getCategoryPrincipale("id");
        $cats = $em->getRepository('DCCoreBundle:Taxonomy')->getRootNodes("id","asc");
        return $this->render('DCCoreBundle:Front/Partials:sidebar.html.twig',array('cats'=>$cats));
    }

    /**
     * @Template
     */
    public function loadRowsByCatAction(Taxonomy $cat) {
        $em = $this->getDoctrine()->getManager();
        $seoPage = $this->container->get('sonata.seo.page');
        $title = $cat->getMetaTitre();
        $keywords = $cat->getMetaKeywords();
        $description = $cat->getMetaDesc();
        $seoPage->setTitle($title)
                ->addMeta('name', 'description', $description)
                ->addMeta('name','keywords',$keywords);
        $rows = $em->getRepository('DCCoreBundle:Row')->getRowsCat($cat,true);

        $first_child = $cat->getFirstChild();
        if($first_child) {
            $pubs = $em->getRepository("DCCoreBundle:PubDetails")->getPubsDetailsByCat($first_child);

        } else {
            $pubs = $em->getRepository("DCCoreBundle:PubDetails")->getPubsDetailsByCat($cat);
        }
        return array('rows'=>$rows,'cat'=>$cat,'pubs'=>$pubs);

    }

    /**
     * @Template
     */
    public function loadRowsByTagAction(Taxonomy $tag) {
        $em = $this->getDoctrine()->getManager();
        $seoPage = $this->container->get('sonata.seo.page');
        $title = $tag->getMetaTitre();
        $keywords = $tag->getMetaKeywords();
        $description = $tag->getMetaDesc();
        $seoPage->setTitle($title)
            ->addMeta('name', 'description', $description)
            ->addMeta('name','keywords',$keywords);
        $sites = $em->getRepository('DCCoreBundle:Site')->getSitesWithThisTag($tag->getId(),true);

            $pubs = $em->getRepository("DCCoreBundle:PubDetails")->getPubsDetailsByCat($tag);

        return $this->render('DCCoreBundle:Front:loadRowsByTag.html.twig',array('sites'=>$sites,'tag'=>$tag,'pubs'=>$pubs));

    }

    public function loadRowsByMarqueAction(Taxonomy $marque) {
        $em = $this->getDoctrine()->getManager();
        $seoPage = $this->container->get('sonata.seo.page');
        $title = $marque->getMetaTitre();
        $keywords = $marque->getMetaKeywords();
        $description = $marque->getMetaDesc();
        $seoPage->setTitle($title)
            ->addMeta('name', 'description', $description)
            ->addMeta('name','keywords',$keywords);
//        $sites = $em->getRepository('DCCoreBundle:Site')->getSitesWithThisTag($marque->getId(),true);
        $finder = $this->get('fos_elastica.finder.app.site');

        $sites = $finder->find($marque->getNom(),1000);

        $pubs = $em->getRepository("DCCoreBundle:PubDetails")->getPubsDetailsByCat($marque);

        return $this->render('DCCoreBundle:Front:loadRowsByMarque.html.twig',array('sites'=>$sites,'marque'=>$marque,'pubs'=>$pubs));

    }

    public function displayMarquesAction() {
        $marques = $this->getDoctrine()->getRepository('DCCoreBundle:Taxonomy')->findMarques(true);
        return $this->render('DCCoreBundle:Front/Partials:displayMarques.html.twig',['marques'=>$marques]);
    }


    public function loadSitesRowByRowAction(Row $row) {
        $em = $this->getDoctrine()->getManager();
        $sitesRow = $em->getRepository('DCCoreBundle:SiteRow')->getSitesInThisRow($row);
        return $this->render('DCCoreBundle:Front/Partials:sitesRow.html.twig',array('sitesRow'=>$sitesRow));
    }

    public function displayBoloboloAction(Row $row) {
        // Compter le nombre de bolobolos de la ligne
            // 1 --> on récupère le nombre sites associés à la row
            $site_rows_total = $row->getSiteRowsCount();
            //2 --> on compte le nombre de sites attendus dans le gabarit
            $gabarit = explode('-',$row->getGabarit()->getName());
            $nb_site_gabarit = count($gabarit);
//            // 3 --> déduction du nbre de bolo => inutile dans ce cas de figure
//            $total_bolobolo = $nb_site_gabarit - $site_rows_total;

            // On commence les bolobolos à partir du tableau gabarit et de l'emplacement total de site.
         // ==> Ex: gabarit 4-4-4 => explodé en tableau ça donne array(4,4,4)
         // ==>     total site 2
        // ==> on découpe le tableau en sous tableau avec array_slice à partir de la val 2 > il reste array(4)
        // -> donc on a 1 seul bolobolo de 4 cols.
            $gabarit_bolobolo = array_slice($gabarit,$site_rows_total);
            // Pour chaque on stocke le nbre de col dans le tableau $bolobolos
            $bolobolos = array();
            foreach($gabarit_bolobolo as $gb) {
                $bolobolos[] = $gb;
            }
        return $this->render('DCCoreBundle:Front/Partials:bolobolo.html.twig',array('bolobolos'=>$bolobolos));

    }

    public function siteOfflinePageAction(Site $site) {

        return $this->render('DCCoreBundle:Front:pageOffline.html.twig',array('site'=>$site));

    }
}
