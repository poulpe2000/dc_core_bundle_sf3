<?php

namespace DC\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class AdminController extends Controller
{
    // Page d'accueil de l'admin
    public function indexAction()
    {
        return $this->render('DCCoreBundle:Admin:index.html.twig');
    }


}