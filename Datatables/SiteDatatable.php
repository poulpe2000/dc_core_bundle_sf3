<?php

namespace DC\CoreBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class SiteDatatable
 *
 * @package DC\CoreBundle\Datatables
 */
class SiteDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = array())
    {
        $this->language->set(array(
            'cdn_language_by_locale' => true
            //'language' => 'de'
        ));

        $this->ajax->set(array(
        ));

        $this->options->set(array(
            'individual_filtering' => true,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
//            'order' => array(array(true, 'asc')),
            'classes' => Style::BOOTSTRAP_3_STYLE, // or Style::BOOTSTRAP_3_STYLE.' table-condensed'
            'paging_type' => Style::FULL_NUMBERS_PAGINATION,
            'state_duration' => 10000,
            'retrieve' => true,


        ));

        $this->features->set(array(
            'state_save' => true
        ));

        $this->columnBuilder

            ->add('domain', Column::class, array(
                'title' => 'Domain',
                ))
            ->add('category.nom', Column::class, array(
                'title' => 'Category Nom',
            ))
            ->add('mainPic.path', ImageColumn::class, array(
                'title' => 'Image',
                'data' => 'mainPic[ ].path',
                'imagine_filter' => 'thumbnail_50_x_50',
                'imagine_filter_enlarged' => 'thumbnail_250_x_250',
                'relative_path' => '/uploads/thumbs',
                'holder_url' => 'https://placehold.it',
                'enlarge' => true,
            ))
//            ->add('mainPic.path', Column::class, array(
//                'title' => 'MainPic Path',
//            ))
            ->add('ancre', Column::class, array(
                'title' => 'Ancre',
            ))

            ->add('isNoFollowAncre', BooleanColumn::class, array(
                'title' => 'IsNoFollowAncre',
                ))



            ->add('created', DateTimeColumn::class, array(
                'title' => 'Created',
                ))




//            ->add(null, ActionColumn::class, array(
//                'title' => $this->translator->trans('sg.datatables.actions.title'),
//                'actions' => array(
//                    array(
//                        'route' => 'site_show',
//                        'route_parameters' => array(
//                            'id' => 'id'
//                        ),
//                        'label' => $this->translator->trans('sg.datatables.actions.show'),
//                        'icon' => 'glyphicon glyphicon-eye-open',
//                        'attributes' => array(
//                            'rel' => 'tooltip',
//                            'title' => $this->translator->trans('sg.datatables.actions.show'),
//                            'class' => 'btn btn-primary btn-xs',
//                            'role' => 'button'
//                        ),
//                    ),
//                    array(
//                        'route' => 'site_edit',
//                        'route_parameters' => array(
//                            'id' => 'id'
//                        ),
//                        'label' => $this->translator->trans('sg.datatables.actions.edit'),
//                        'icon' => 'glyphicon glyphicon-edit',
//                        'attributes' => array(
//                            'rel' => 'tooltip',
//                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
//                            'class' => 'btn btn-primary btn-xs',
//                            'role' => 'button'
//                        ),
//                    )
//                )
//            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'DC\CoreBundle\Entity\Site';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'site_datatable';
    }
}
