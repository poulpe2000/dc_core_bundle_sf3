<?php

namespace DC\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Row
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DC\CoreBundle\Entity\RowRepository")
 */
class Row
{
    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Taxonomy")
     * @ORM\JoinColumn(name="id_cat", referencedColumnName="id",onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Gabarit")
     * @ORM\JoinColumn(name="id_gabarit", referencedColumnName="id",onDelete="CASCADE")
     */
    private $gabarit;

    /**
     * @ORM\OneToMany(targetEntity="DC\CoreBundle\Entity\SiteRow",mappedBy="row")
     */
    private $siteRows;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_complet", type="boolean")
     */
    private $isComplet;




    /**
     * @var boolean
     * @ORM\Column(name="is_online",type="boolean")
     */
    private $isOnline;

    /**
     * @var integer
     * @ORM\Column(name="rank", type="integer")
     */
    private $rank;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isComplet
     *
     * @param boolean $isComplet
     * @return Row
     */
    public function setIsComplet($isComplet)
    {
        $this->isComplet = $isComplet;

        return $this;
    }

    /**
     * Get isComplet
     *
     * @return boolean 
     */
    public function getIsComplet()
    {
        return $this->isComplet;
    }

    /**
     * Set category
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $category
     * @return Row
     */
    public function setCategory(\DC\CoreBundle\Entity\Taxonomy $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \DC\CoreBundle\Entity\Taxonomy
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set gabarit
     *
     * @param \DC\CoreBundle\Entity\Gabarit $gabarit
     * @return Row
     */
    public function setGabarit(\DC\CoreBundle\Entity\Gabarit $gabarit = null)
    {
        $this->gabarit = $gabarit;

        return $this;
    }

    /**
     * Get gabarit
     *
     * @return \DC\CoreBundle\Entity\Gabarit
     */
    public function getGabarit()
    {
        return $this->gabarit;
    }

    /**
     * Set isOnline
     *
     * @param boolean $isOnline
     * @return Row
     */
    public function setIsOnline($isOnline)
    {
        $this->isOnline = $isOnline;

        return $this;
    }

    /**
     * Get isOnline
     *
     * @return boolean 
     */
    public function getIsOnline()
    {
        return $this->isOnline;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Row
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Row
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->siteRow = new \Doctrine\Common\Collections\ArrayCollection();
    }

  

    /**
     * Add siteRows
     *
     * @param \DC\CoreBundle\Entity\SiteRow $siteRows
     * @return Row
     */
    public function addSiteRow(\DC\CoreBundle\Entity\SiteRow $siteRows)
    {
        $this->siteRows[] = $siteRows;

        return $this;
    }

    /**
     * Remove siteRows
     *
     * @param \DC\CoreBundle\Entity\SiteRow $siteRows
     */
    public function removeSiteRow(\DC\CoreBundle\Entity\SiteRow $siteRows)
    {
        $this->siteRows->removeElement($siteRows);
    }

    /**
     * Get siteRows
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSiteRows()
    {
        return $this->siteRows;
    }

    public function getSiteRowsCount()
    {
        return $this->siteRows->count();
    }

    /**
     * Set rank
     *
     * @param integer $rank
     * @return Row
     */
    public function setRank($rank)
    {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer 
     */
    public function getRank()
    {
        return $this->rank;
    }
}
