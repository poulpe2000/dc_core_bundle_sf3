<?php

namespace DC\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Text
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DC\CoreBundle\Entity\TextRepository")
 */
class Text
{
    /**
     * @ORM\ManyToMany(targetEntity="DC\CoreBundle\Entity\Taxonomy", cascade={"persist"})
     * @ORM\JoinTable(name="text_tags")
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Taxonomy")
     * @ORM\JoinColumn(name="id_cat", referencedColumnName="id",onDelete="CASCADE")
     */
    private $category;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_titre", type="string", length=255)
     */
    private $metaTitre;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_desc", type="text")
     */
    private $metaDesc;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text")
     */
    private $metaKeywords;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Text
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Text
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set metaTitre
     *
     * @param string $metaTitre
     * @return Text
     */
    public function setMetaTitre($metaTitre)
    {
        $this->metaTitre = $metaTitre;

        return $this;
    }

    /**
     * Get metaTitre
     *
     * @return string 
     */
    public function getMetaTitre()
    {
        return $this->metaTitre;
    }

    /**
     * Set metaDesc
     *
     * @param string $metaDesc
     * @return Text
     */
    public function setMetaDesc($metaDesc)
    {
        $this->metaDesc = $metaDesc;

        return $this;
    }

    /**
     * Get metaDesc
     *
     * @return string 
     */
    public function getMetaDesc()
    {
        return $this->metaDesc;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Text
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add tags
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $tags
     * @return Text
     */
    public function addTag(\DC\CoreBundle\Entity\Taxonomy $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $tags
     */
    public function removeTag(\DC\CoreBundle\Entity\Taxonomy $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set category
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $category
     * @return Text
     */
    public function setCategory(\DC\CoreBundle\Entity\Taxonomy $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \DC\CoreBundle\Entity\Taxonomy
     */
    public function getCategory()
    {
        return $this->category;
    }
}
