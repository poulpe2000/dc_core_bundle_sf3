<?php

namespace DC\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PubDetails
 *
 * @ORM\Table(name="pub_details")
 * @ORM\Entity(repositoryClass="DC\CoreBundle\Repository\PubDetailsRepository")
 */
class PubDetails
{
    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Taxonomy")
     * @ORM\JoinColumn(name="id_cat", referencedColumnName="id",onDelete="SET NULL")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Pub", inversedBy="pubDetails")
     * @ORM\JoinColumn(name="id_pub", referencedColumnName="id",onDelete="CASCADE")
     */
    private $pub;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_online", type="boolean")
     */
    private $isOnline;

    /**
     * @var string
     *
     * @ORM\Column(name="url_destination", type="string", length=255)
     */
    private $urlDestination;

    /**
     * @var string
     *
     * @ORM\Column(name="code_couleur_bkg", type="string", length=255,nullable=true)
     */
    private $codeCouleurBkg;

    /**
     * @var string
     *
     * @ORM\Column(name="duree_affichage", type="integer",nullable=true)
     */
    private $dureeAffichage;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isOnline
     *
     * @param boolean $isOnline
     *
     * @return PubDetails
     */
    public function setIsOnline($isOnline)
    {
        $this->isOnline = $isOnline;

        return $this;
    }

    /**
     * Get isOnline
     *
     * @return bool
     */
    public function getIsOnline()
    {
        return $this->isOnline;
    }

    /**
     * Set urlDestination
     *
     * @param string $urlDestination
     *
     * @return PubDetails
     */
    public function setUrlDestination($urlDestination)
    {
        $this->urlDestination = $urlDestination;

        return $this;
    }

    /**
     * Get urlDestination
     *
     * @return string
     */
    public function getUrlDestination()
    {
        return $this->urlDestination;
    }

    /**
     * Set category
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $category
     *
     * @return PubDetails
     */
    public function setCategory(\DC\CoreBundle\Entity\Taxonomy $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \DC\CoreBundle\Entity\Taxonomy
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set pub
     *
     * @param \DC\CoreBundle\Entity\Pub $pub
     *
     * @return PubDetails
     */
    public function setPub(\DC\CoreBundle\Entity\Pub $pub = null)
    {
        $this->pub = $pub;

        return $this;
    }

    /**
     * Get pub
     *
     * @return \DC\CoreBundle\Entity\Pub
     */
    public function getPub()
    {
        return $this->pub;
    }

    /**
     * Set codeCouleurBkg
     *
     * @param string $codeCouleurBkg
     *
     * @return PubDetails
     */
    public function setCodeCouleurBkg($codeCouleurBkg)
    {
        $this->codeCouleurBkg = $codeCouleurBkg;

        return $this;
    }

    /**
     * Get codeCouleurBkg
     *
     * @return string
     */
    public function getCodeCouleurBkg()
    {
        return $this->codeCouleurBkg;
    }

    /**
     * Set dureeAffichage
     *
     * @param integer $dureeAffichage
     *
     * @return PubDetails
     */
    public function setDureeAffichage($dureeAffichage)
    {
        $this->dureeAffichage = $dureeAffichage;

        return $this;
    }

    /**
     * Get dureeAffichage
     *
     * @return integer
     */
    public function getDureeAffichage()
    {
        return $this->dureeAffichage;
    }
}
