<?php

namespace DC\CoreBundle\Entity;

use DC\CoreBundle\Form\TaxonomyType;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Taxonomy
 *
 * @Gedmo\Tree(type="nested")
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DC\CoreBundle\Entity\TaxonomyRepository")
 */
class Taxonomy
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_tag", type="boolean",nullable=true)
     */
    private $isTag;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_marque", type="boolean",nullable=true)
     */
    private $isMarque;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_marque_online", type="boolean",nullable=true)
     */
    private $isMarqueOnline;

    /**
     * @ORM\Column(name="description",type="text",nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(name="meta_titre",type="text")
     */
    private $metaTitre;

    /**
     * @ORM\Column(name="meta_desc",type="text")
     */
    private $metaDesc;

    /**
     * @ORM\Column(name="meta_keywords",type="text")
     */
    private $metaKeywords;

    /**
     * @ORM\OneToOne(targetEntity="DC\CoreBundle\Entity\Pics", cascade={"persist", "remove"}, inversedBy="taxonomy")
     * @ORM\JoinColumn(name="id_ico", referencedColumnName="id")
     */
    private $ico;

    /**
     * @Gedmo\Slug(fields={"nom"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Taxonomy")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Taxonomy", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Taxonomy", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;

    public function __toString()
    {
        return $this->getIndentedName();
    }

    // Attribut non mappé avec l'orm, juste pour l'affichage des formulaires
    private $indentedName;

    public function getIndentedName()
    {
        return $this->indentedName = str_repeat(" ===> ", $this->lvl) . $this->getNom();
    }

    public function getIndentedNameView() {
        return str_repeat($this->parent." > ", $this->lvl) . $this->getNom();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Taxonomy
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Taxonomy
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Taxonomy
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Taxonomy
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Taxonomy
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Taxonomy
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metaTitre
     *
     * @param string $metaTitre
     * @return Taxonomy
     */
    public function setMetaTitre($metaTitre)
    {
        $this->metaTitre = $metaTitre;

        return $this;
    }

    /**
     * Get metaTitre
     *
     * @return string
     */
    public function getMetaTitre()
    {
        return $this->metaTitre;
    }

    /**
     * Set metaDesc
     *
     * @param string $metaDesc
     * @return Taxonomy
     */
    public function setMetaDesc($metaDesc)
    {
        $this->metaDesc = $metaDesc;

        return $this;
    }

    /**
     * Get metaDesc
     *
     * @return string
     */
    public function getMetaDesc()
    {
        return $this->metaDesc;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Taxonomy
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }


    /**
     * Set ico
     *
     * @param \DC\CoreBundle\Entity\Pics $ico
     * @return Taxonomy
     */
    public function setIco(\DC\CoreBundle\Entity\Pics $ico = null)
    {
        $this->ico = $ico;

        return $this;
    }

    /**
     * Get ico
     *
     * @return \DC\CoreBundle\Entity\Pics
     */
    public function getIco()
    {
        return $this->ico;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set lft
     *
     * @param integer $lft
     * @return Taxonomy
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     * @return Taxonomy
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     * @return Taxonomy
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set root
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $root
     * @return Taxonomy
     */
    public function setRoot(\DC\CoreBundle\Entity\Taxonomy $root = null)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return \DC\CoreBundle\Entity\Taxonomy
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set parent
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $parent
     * @return Taxonomy
     */
    public function setParent(\DC\CoreBundle\Entity\Taxonomy $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \DC\CoreBundle\Entity\Taxonomy
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add children
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $children
     * @return Taxonomy
     */
    public function addChild(\DC\CoreBundle\Entity\Taxonomy $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $children
     */
    public function removeChild(\DC\CoreBundle\Entity\Taxonomy $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * get a numeric representation of the press category level
     * i.e. parent categories - 0, child categories - 1, grandchild categories - 2 etc...
     *
     * @return int
     */
    public function getLevel()
    {
        $level = 0;

        $category = $this;

        // parent category
        if ($category->hasParent() === false) {
            return $level;
        }

        while ($category = $category->getParent()) {
            $level++;
        }

        return $level;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        return ($this->parent != null ? true : false);
    }

    /**
     * @param mixed $children
     */
    public function getFirstChild()
    {
        $category = $this;
        $level = 0;
        $cat_top = false;
        if ($category->hasParent() === true) {
            $cat_top = null;
            while ($category = $category->getParent()) {
                $cat_top = $category;
                $level++;
            }
        }
        return $cat_top;
    }




    /**
     * Set isTag
     *
     * @param boolean $isTag
     *
     * @return Taxonomy
     */
    public function setIsTag($isTag)
    {
        $this->isTag = $isTag;

        return $this;
    }

    /**
     * Get isTag
     *
     * @return boolean
     */
    public function getIsTag()
    {
        return $this->isTag;
    }

    /**
     * Set isMarque
     *
     * @param boolean $isMarque
     *
     * @return Taxonomy
     */
    public function setIsMarque($isMarque)
    {
        $this->isMarque = $isMarque;

        return $this;
    }

    /**
     * Get isMarque
     *
     * @return boolean
     */
    public function getIsMarque()
    {
        return $this->isMarque;
    }

    /**
     * Set isMarqueOnline
     *
     * @param boolean $isMarqueOnline
     *
     * @return Taxonomy
     */
    public function setIsMarqueOnline($isMarqueOnline)
    {
        $this->isMarqueOnline = $isMarqueOnline;

        return $this;
    }

    /**
     * Get isMarqueOnline
     *
     * @return boolean
     */
    public function getIsMarqueOnline()
    {
        return $this->isMarqueOnline;
    }
}
