<?php

namespace DC\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
//use \JsonSerializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Site
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DC\CoreBundle\Entity\SiteRepository")
 * @UniqueEntity("url",message="Un site avec cette url est déjà présent dans la base")
 *
 */
class Site
{
    /**
     * @ORM\ManyToMany(targetEntity="DC\CoreBundle\Entity\Taxonomy", cascade={"persist"})
     * @ORM\JoinTable(name="site_tags")
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Taxonomy")
     * @ORM\JoinColumn(name="id_cat", referencedColumnName="id",onDelete="CASCADE")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="DC\CoreBundle\Entity\Pics",mappedBy="site")
     */
    private $pics;

    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Pics")
     * @ORM\JoinColumn(name="id_main_pic", referencedColumnName="id",onDelete="SET NULL")
     */
    private $mainPic;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="url_error", type="string", length=255,nullable=true)
     */
    private $urlError;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_short", type="text")
     */
    private $descShort;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=255,nullable=true)
     */
    private $domain;



    /**
     * @var boolean
     *
     * @ORM\Column(name="is_no_follow_ancre", type="boolean")
     */
    private $isNoFollowAncre;

    /**
     * @var string
     *
     * @ORM\Column(name="ancre", type="string", length=255)
     */
    private $ancre;




    /**
     * @var boolean
     *
     * @ORM\Column(name="is_offline", type="boolean",nullable=true)
     */
    private $isOffline;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_desc_short", type="boolean",nullable=true)
     */
    private $showDescShort;
    
    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @Gedmo\Slug(fields={"domain"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Site
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set descShort
     *
     * @param string $descShort
     * @return Site
     */
    public function setDescShort($descShort)
    {
        $this->descShort = $descShort;

        return $this;
    }

    /**
     * Get descShort
     *
     * @return string 
     */
    public function getDescShort()
    {
        return $this->descShort;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Site
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }



    /**
     * Set isNoFollowAncre
     *
     * @param boolean $isNoFollowAncre
     * @return Site
     */
    public function setIsNoFollowAncre($isNoFollowAncre)
    {
        $this->isNoFollowAncre = $isNoFollowAncre;

        return $this;
    }

    /**
     * Get isNoFollowAncre
     *
     * @return boolean 
     */
    public function getIsNoFollowAncre()
    {
        return $this->isNoFollowAncre;
    }

    /**
     * Set ancre
     *
     * @param string $ancre
     * @return Site
     */
    public function setAncre($ancre)
    {
        $this->ancre = $ancre;

        return $this;
    }

    /**
     * Get ancre
     *
     * @return string 
     */
    public function getAncre()
    {
        return $this->ancre;
    }

  


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isOffline = false;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Site
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Site
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Site
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add tags
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $tags
     * @return Site
     */
    public function addTag(\DC\CoreBundle\Entity\Taxonomy $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $tags
     */
    public function removeTag(\DC\CoreBundle\Entity\Taxonomy $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set category
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $category
     * @return Site
     */
    public function setCategory(\DC\CoreBundle\Entity\Taxonomy $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \DC\CoreBundle\Entity\Taxonomy
     */
    public function getCategory()
    {
        return $this->category;
    }

    // Sérializer un objet
    public function jsonSerialize()
    {
        return array(
            'url' => $this->url,
            'domain'=> $this->domain,
            'ancre'=> $this->ancre,
            'tags'=> array($this->getTags),
            'category'=> $this->category
        );
    }




    /**
     * Add pics
     *
     * @param \DC\CoreBundle\Entity\Pics $pics
     * @return Site
     */
    public function addPic(\DC\CoreBundle\Entity\Pics $pics)
    {
        $this->pics[] = $pics;

        return $this;
    }

    /**
     * Remove pics
     *
     * @param \DC\CoreBundle\Entity\Pics $pics
     */
    public function removePic(\DC\CoreBundle\Entity\Pics $pics)
    {
        $this->pics->removeElement($pics);
    }

    /**
     * Get pics
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPics()
    {
        return $this->pics;
    }



    /**
     * Set mainPic
     *
     * @param \DC\CoreBundle\Entity\Pics $mainPic
     * @return Site
     */
    public function setMainPic(\DC\CoreBundle\Entity\Pics $mainPic = null)
    {
        $this->mainPic = $mainPic;

        return $this;
    }

    /**
     * Get mainPic
     *
     * @return \DC\CoreBundle\Entity\Pics
     */
    public function getMainPic()
    {
        return $this->mainPic;
    }

    /**
     * Set isOffline
     *
     * @param boolean $isOffline
     * @return Site
     */
    public function setIsOffline($isOffline)
    {
        $this->isOffline = $isOffline;

        return $this;
    }

    /**
     * Get isOffline
     *
     * @return boolean 
     */
    public function getIsOffline()
    {
        return $this->isOffline;
    }




    /**
     * Set urlError
     *
     * @param string $urlError
     * @return Site
     */
    public function setUrlError($urlError)
    {
        $this->urlError = $urlError;

        return $this;
    }

    /**
     * Get urlError
     *
     * @return string 
     */
    public function getUrlError()
    {
        return $this->urlError;
    }

    /**
     * Set showDescShort
     *
     * @param boolean $showDescShort
     * @return Site
     */
    public function setShowDescShort($showDescShort)
    {
        $this->showDescShort = $showDescShort;

        return $this;
    }

    /**
     * Get showDescShort
     *
     * @return boolean 
     */
    public function getShowDescShort()
    {
        return $this->showDescShort;
    }
}
