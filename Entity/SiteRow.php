<?php

namespace DC\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SiteRow
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DC\CoreBundle\Entity\SiteRowRepository")
 */
class SiteRow
{
    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Row",inversedBy="siteRows")
     * @ORM\JoinColumn(name="id_row",referencedColumnName="id",onDelete="CASCADE")
     */
    private $row;

    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Site")
     * @ORM\JoinColumn(name="id_site",referencedColumnName="id",onDelete="CASCADE")
     */
    private $site;

    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Text")
     * @ORM\JoinColumn(name="id_text",referencedColumnName="id",onDelete="CASCADE")
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Color")
     * @ORM\JoinColumn(name="id_color",referencedColumnName="id",onDelete="CASCADE")
     */
    private $color;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="nb_cols", type="integer")
     */
    private $nbCols;



    /**
     * @var boolean
     *
     * @ORM\Column(name="is_bolobolo", type="boolean")
     */
    private $isBolobolo;


    /**
     * @var integer
     * @ORM\Column(name="position", type="integer", nullable = true)
     */
    private $position;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbCols
     *
     * @param integer $nbCols
     * @return SiteRow
     */
    public function setNbCols($nbCols)
    {
        $this->nbCols = $nbCols;

        return $this;
    }

    /**
     * Get nbCols
     *
     * @return integer 
     */
    public function getNbCols()
    {
        return $this->nbCols;
    }

    /**
     * Set isBolobolo
     *
     * @param boolean $isBolobolo
     * @return SiteRow
     */
    public function setIsBolobolo($isBolobolo)
    {
        $this->isBolobolo = $isBolobolo;

        return $this;
    }

    /**
     * Get isBolobolo
     *
     * @return boolean 
     */
    public function getIsBolobolo()
    {
        return $this->isBolobolo;
    }

    /**
     * Set row
     *
     * @param \DC\CoreBundle\Entity\Row $row
     * @return SiteRow
     */
    public function setRow(\DC\CoreBundle\Entity\Row $row = null)
    {
        $this->row = $row;

        return $this;
    }

    /**
     * Get row
     *
     * @return \DC\CoreBundle\Entity\Row
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set site
     *
     * @param \DC\CoreBundle\Entity\Site $site
     * @return SiteRow
     */
    public function setSite(\DC\CoreBundle\Entity\Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return \DC\CoreBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return SiteRow
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Set color
     *
     * @param \DC\CoreBundle\Entity\Color $color
     * @return SiteRow
     */
    public function setColor(\DC\CoreBundle\Entity\Color $color = null)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return \DC\CoreBundle\Entity\Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set text
     *
     * @param \DC\CoreBundle\Entity\Text $text
     * @return SiteRow
     */
    public function setText(\DC\CoreBundle\Entity\Text $text = null)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return \DC\CoreBundle\Entity\Text
     */
    public function getText()
    {
        return $this->text;
    }
}
