<?php

namespace DC\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Pics
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="DC\CoreBundle\Entity\PicsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Pics
{
    /**
     * @ORM\ManyToOne(targetEntity="DC\CoreBundle\Entity\Site",inversedBy="pics")
     * @ORM\JoinColumn(name="id_site", referencedColumnName="id",onDelete="SET NULL")
     */
    private $site;

    /**
     * @ORM\OneToOne(targetEntity="DC\CoreBundle\Entity\Taxonomy", mappedBy="ico")
     */
    private $taxonomy;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255,nullable=true)
     */
    private $path;


    /**
     * @Assert\File(
     *     maxSize = "1024k",
     *     maxSizeMessage = "Votre fichier ne doit pas dépasser 1Mo",
     *     mimeTypes = {"image/jpeg","image/png","image/gif"},
     *     mimeTypesMessage = "Merci d'uploader un fichier jpg, png ou gif"
     * )
     */
    private $file;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_cropped", type="boolean",options={"default": false},nullable=true)
     */
    private $isCropped;


    /**
     * @var boolean
     *
     * @ORM\Column(name="is_main", type="boolean",nullable=true)
     */
    private $isMain;

    /**
     * @var string
     *
     * @ORM\Column(name="update_rand", type="string", length=255,nullable=true)
     */
    private $update_rand;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Pics
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set isMain
     *
     * @param boolean $isMain
     * @return Pics
     */
    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * Get isMain
     *
     * @return boolean 
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * Set site
     *
     * @param \DC\CoreBundle\Entity\Site $site
     * @return Pics
     */
    public function setSite(\DC\CoreBundle\Entity\Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return \DC\CoreBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set isCropped
     *
     * @param boolean $isCropped
     * @return Pics
     */
    public function setIsCropped($isCropped)
    {
        $this->isCropped = $isCropped;

        return $this;
    }

    /**
     * Get isCropped
     *
     * @return boolean 
     */
    public function getIsCropped()
    {
        return $this->isCropped;
    }

    /**
     * Get mediaFile
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return this
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }
    // retourner le chemin absolu du fichier
    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    // permet de retourner le chemin web, qui lui peut être utilisé dans un template pour ajouter un lien vers le fichier uploadé
    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        // le chemin absolu du répertoire où les documents uploadés doivent être sauvegardés
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // on se débarrasse de « __DIR__ » afin de ne pas avoir de problème lorsqu'on affiche
        // le document/image dans la vue.
        return 'uploads/thumbs';
    }
    protected function getNoIcoRootDir() {
        // le chemin absolu du répertoire où les documents uploadés doivent être sauvegardés
        return __DIR__.'/../../../../web/bundles/dccore/images';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {

        if( $this->file === "screen" or $this->file === "crop") {
            return null;
        }
        else if (null !== $this->file ) {
            // faites ce que vous voulez pour générer un nom unique
            $this->path = sha1(uniqid(mt_rand(), true)).'.'.$this->file->guessExtension();

        }
        else if(null === $this->update_rand) {
            $this->path = "no_ico.png";
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {

        if (null === $this->file or $this->file === "screen" or $this->file === "crop") {
            return;
        }

        // s'il y a une erreur lors du déplacement du fichier, une exception
        // va automatiquement être lancée par la méthode move(). Cela va empêcher
        // proprement l'entité d'être persistée dans la base de données si
        // erreur il y a
        $this->file->move($this->getUploadRootDir(), $this->path);

        unset($this->file);
    }


    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        if ($this->path = "no_ico.png") {
            return null;
        } else if ($file = $this->getAbsolutePath()) {
            unlink($file);
        }

    }

    /**
     * Set taxonomy
     *
     * @param \DC\CoreBundle\Entity\Taxonomy $taxonomy
     * @return Pics
     */
    public function setTaxonomy(\DC\CoreBundle\Entity\Taxonomy $taxonomy = null)
    {
        $this->taxonomy = $taxonomy;

        return $this;
    }

    /**
     * Get taxonomy
     *
     * @return \DC\CoreBundle\Entity\Taxonomy
     */
    public function getTaxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * Set update_rand
     *
     * @param string $updateRand
     * @return Pics
     */
    public function setUpdateRand($updateRand)
    {
        $this->update_rand = $updateRand;

        return $this;
    }

    /**
     * Get update_rand
     *
     * @return string 
     */
    public function getUpdateRand()
    {
        return $this->update_rand;
    }
}
