<?php

namespace DC\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Setting
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Setting
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="site_name", type="string", length=255)
     */
    private $siteName;

    /**
     * @var string
     *
     * @ORM\Column(name="full_url", type="string", length=255)
     */
    private $full_url;

    /**
     * @var string
     *
     * @ORM\Column(name="site_name_short", type="string", length=255)
     */
    private $siteNameShort;

    /**
     * @var string
     *
     * @ORM\Column(name="site_slogan", type="string", length=255)
     */
    private $siteSlogan;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=255)
     */
    private $metaTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text")
     */
    private $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="text")
     */
    private $metaKeywords;

    /**
     * @var boolean
     * @ORM\Column(name="is_no_index", type="boolean",nullable=true)
     */
    private $isNoIndex;

    /**
     * @var string
     *
     * @ORM\Column(name="ga_code", type="string", length=255,nullable=true
     * )
     */
    private $gaCode;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set siteName
     *
     * @param string $siteName
     * @return Setting
     */
    public function setSiteName($siteName)
    {
        $this->siteName = $siteName;

        return $this;
    }

    /**
     * Get siteName
     *
     * @return string 
     */
    public function getSiteName()
    {
        return $this->siteName;
    }

    /**
     * Set siteSlogan
     *
     * @param string $siteSlogan
     * @return Setting
     */
    public function setSiteSlogan($siteSlogan)
    {
        $this->siteSlogan = $siteSlogan;

        return $this;
    }

    /**
     * Get siteSlogan
     *
     * @return string 
     */
    public function getSiteSlogan()
    {
        return $this->siteSlogan;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     * @return Setting
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string 
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     * @return Setting
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string 
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * Set metaKeywords
     *
     * @param string $metaKeywords
     * @return Setting
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    /**
     * Get metaKeywords
     *
     * @return string 
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * Set siteNameShort
     *
     * @param string $siteNameShort
     * @return Setting
     */
    public function setSiteNameShort($siteNameShort)
    {
        $this->siteNameShort = $siteNameShort;

        return $this;
    }

    /**
     * Get siteNameShort
     *
     * @return string 
     */
    public function getSiteNameShort()
    {
        return $this->siteNameShort;
    }

    /**
     * Set full_url
     *
     * @param string $fullUrl
     * @return Setting
     */
    public function setFullUrl($fullUrl)
    {
        $this->full_url = $fullUrl;

        return $this;
    }

    /**
     * Get full_url
     *
     * @return string 
     */
    public function getFullUrl()
    {
        return $this->full_url;
    }

    /**
     * Set isNoIndex
     *
     * @param boolean $isNoIndex
     * @return Setting
     */
    public function setIsNoIndex($isNoIndex)
    {
        $this->isNoIndex = $isNoIndex;

        return $this;
    }

    /**
     * Get isNoIndex
     *
     * @return boolean 
     */
    public function getIsNoIndex()
    {
        return $this->isNoIndex;
    }

    /**
     * Set gaCode
     *
     * @param string $gaCode
     * @return Setting
     */
    public function setGaCode($gaCode)
    {
        $this->gaCode = $gaCode;

        return $this;
    }

    /**
     * Get gaCode
     *
     * @return string 
     */
    public function getGaCode()
    {
        return $this->gaCode;
    }
}
