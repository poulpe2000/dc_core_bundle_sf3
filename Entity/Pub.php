<?php

namespace DC\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Pub
 *
 * @ORM\Table(name="pub")
 * @ORM\Entity(repositoryClass="DC\CoreBundle\Repository\PubRepository")
 */
class Pub
{
    /**
     * @ORM\OneToOne(targetEntity="DC\CoreBundle\Entity\Pics", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="id_pic_pub", referencedColumnName="id")
     */
    private $picPub;

    /**
     * @ORM\OneToOne(targetEntity="DC\CoreBundle\Entity\Pics", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="id_pic_pub_mobile", referencedColumnName="id")
     */
    private $picPubMobile;

    /**
     * @ORM\OneToMany(targetEntity="DC\CoreBundle\Entity\PubDetails",mappedBy="pub")
     */
    private $pubDetails;



    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var boolean
     * @ORM\Column(name="is_delete",type="boolean",nullable=true)
     */
    private $isDelete;

    /**
     * @var string
     *
     * @ORM\Column(name="code_couleur", type="string", length=255, nullable=true)
     */
    private $codeCouleur;

    
    /**
     * @var \DateTime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var \DateTime $updated
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Pub
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Pub
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Pub
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }


    /**
     * Set picPub
     *
     * @param \DC\CoreBundle\Entity\Pics $picPub
     *
     * @return Pub
     */
    public function setPicPub(\DC\CoreBundle\Entity\Pics $picPub = null)
    {
        $this->picPub = $picPub;

        return $this;
    }

    /**
     * Get picPub
     *
     * @return \DC\CoreBundle\Entity\Pics
     */
    public function getPicPub()
    {
        return $this->picPub;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pubDetails = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pubDetail
     *
     * @param \DC\CoreBundle\Entity\PubDetails $pubDetail
     *
     * @return Pub
     */
    public function addPubDetail(\DC\CoreBundle\Entity\PubDetails $pubDetail)
    {
        $this->pubDetails[] = $pubDetail;

        return $this;
    }

    /**
     * Remove pubDetail
     *
     * @param \DC\CoreBundle\Entity\PubDetails $pubDetail
     */
    public function removePubDetail(\DC\CoreBundle\Entity\PubDetails $pubDetail)
    {
        $this->pubDetails->removeElement($pubDetail);
    }

    /**
     * Get pubDetails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPubDetails()
    {
        return $this->pubDetails;
    }

    /**
     * Set isDelete
     *
     * @param boolean $isDelete
     *
     * @return Pub
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;

        return $this;
    }

    /**
     * Get isDelete
     *
     * @return boolean
     */
    public function getIsDelete()
    {
        return $this->isDelete;
    }

    /**
     * Set codeCouleur
     *
     * @param string $codeCouleur
     *
     * @return Pub
     */
    public function setCodeCouleur($codeCouleur)
    {
        $this->codeCouleur = $codeCouleur;

        return $this;
    }

    /**
     * Get codeCouleur
     *
     * @return string
     */
    public function getCodeCouleur()
    {
        return $this->codeCouleur;
    }

    /**
     * Set picPubMobile
     *
     * @param \DC\CoreBundle\Entity\Pics $picPubMobile
     *
     * @return Pub
     */
    public function setPicPubMobile(\DC\CoreBundle\Entity\Pics $picPubMobile = null)
    {
        $this->picPubMobile = $picPubMobile;

        return $this;
    }

    /**
     * Get picPubMobile
     *
     * @return \DC\CoreBundle\Entity\Pics
     */
    public function getPicPubMobile()
    {
        return $this->picPubMobile;
    }
}
