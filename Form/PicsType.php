<?php

namespace DC\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PicsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('file',
                FileType::class,array('label'=>"Uploadez votre image",'required'=>false,'attr'=>array('class'=>'form-control')));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DC\CoreBundle\Entity\Pics'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'dc_adminbundle_pics';
    }
}
