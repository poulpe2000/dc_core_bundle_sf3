<?php

namespace DC\CoreBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
class SiteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
//            ->add('url', 'autocomplete', array('class' => 'DCCoreBundle:Site'))
            ->add('url',TextType::class,array('attr'=>array('class'=>'form-control','data-id' =>'url_autoc','data-url'=>"json/search/url")));

            // Si on est en mode créa
            if($builder->getData()->getId() == false) {
                $crea =true;
                $builder->add('isNoFollowAncre',ChoiceType::class,array('label'=>'Lien "voir le site" noFollow ?','expanded'=>true,
                    'data' => 1,
                    'choices' => array("oui" => true, 'non' => false),
                    'choices_as_values' => true));
            }
            // Sinon mode edit
            else {
                $crea =false;
                if($builder->getData()->getIsOffline()==true) {
                    $builder
                        ->add('urlError',TextType::class,array('label'=>'url en erreur','attr'=>array('class'=>'form-control')));
                }
                    $builder->add('isNoFollowAncre',ChoiceType::class,array('label'=>'Lien "voir le site" noFollow ?','expanded'=>true,
                        'choices' => array("oui" => true, 'non' => false),
                        'choices_as_values' => true))
                    ->add('isOffline',ChoiceType::class,array('label'=>'Mettre le site offline ?','expanded'=>true,
                        'choices' => array("oui" => true, 'non' => false),
                        'choices_as_values' => true));
            }
            $builder->add('descShort',TextareaType::class,array('label'=>"Description du site",'attr'=>array('class'=>'form-control','rows' => 10)));

        if($crea) {
            $builder->add('showDescShort',ChoiceType::class,array('label'=>'Afficher la description ?','expanded'=>true,
                'data' => 1,
                'choices' => array("oui" => true, 'non' => false),
                'choices_as_values' => true));
        } else {
            $builder->add('showDescShort',ChoiceType::class,array('label'=>'Afficher la description ?','expanded'=>true,

                'choices' => array("oui" => true, 'non' => false),
                'choices_as_values' => true));
        }



           $builder->add('ancre',TextType::class,array('label'=>'Ancre du lien','attr'=>array('class'=>'form-control')))
            ->add('tags',EntityType::class,array('label'=>'Tags',
                                        'attr'=> array('class'=>'form-control chosen-select'),
                                        'class'=>"DC\CoreBundle\Entity\Taxonomy",
                                        'choice_label'=>'nom',
                                        'multiple'=>true,
                                        'required'=>false))
            ->add('category',EntityType::class,array('label'=>'Catégorie principale',
                                            'attr'=>array('class'=>'form-control'),
                                            'class'=>"DC\CoreBundle\Entity\Taxonomy",
                                            "choice_label"=>"nom"))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DC\CoreBundle\Entity\Site'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'dc_adminbundle_site';
    }
}
