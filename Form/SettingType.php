<?php

namespace DC\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
class SettingType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('siteName',TextType::class,array('label'=>'Nom de l\'annuaire','attr'=>array('class'=>'form-control')))
            ->add('siteNameShort',TextType::class,array('label'=>'Nom de l\'annuaire (version courte)','attr'=>array('class'=>'form-control')))
            ->add('full_url',TextType::class,array('label'=>'Url complète. Ex: http://www.laine-et-maille.com','attr'=>array('class'=>'form-control')))
            ->add('siteSlogan',TextType::class,array('label'=>'Slogan de l\'annuaire','attr'=>array('class'=>'form-control')))
            ->add('gaCode',TextType::class,array('label'=>'Code Google Analytics','required'=>false,'attr'=>array('class'=>'form-control')))
            ->add('metaTitle',TextareaType::class,array('label'=>'META Titre principal','attr'=>array('class'=>'form-control')))
            ->add('metaDescription',TextareaType::class,array('label'=>'META Description principal','attr'=>array('class'=>'form-control')))
            ->add('metaKeywords',TextareaType::class,array('label'=>'META keywords principal','attr'=>array('class'=>'form-control')));
        if($builder->getData()->getId() == false) {

            $builder->add(
                'isNoIndex',
                ChoiceType::class,
                array(
                    'label' => 'Bloquer les moteurs de recherche ?',
                    'expanded' => true,
                    'data' => 0,
                    'choices' => array("oui" => true, 'non' => false),
                    'choices_as_values' => true
                )
            );
        } else {
            $builder->add(
                'isNoIndex',
                ChoiceType::class,
                array(
                    'label' => 'Bloquer les moteurs de recherche ?',
                    'expanded' => true,
                    'choices' => array("oui" => true, 'non' => false),
                    'choices_as_values' => true
                )
            );
        }

    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DC\CoreBundle\Entity\Setting'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'dc_adminbundle_setting';
    }
}
