<?php

namespace DC\CoreBundle\Form;

use DC\CoreBundle\Entity\TaxonomyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
class TaxonomyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,array('attr'=>array('class'=>'form-control')))
            ->add('parent',EntityType::class,array('label'=>'Parent de la catégorie','attr'=>array('class'=>'form-control'),
                'class'=>'DC\CoreBundle\Entity\Taxonomy','placeholder' => 'Pas de rubrique parente',
                'required'=>false,'choice_label'=>'indentedName',"query_builder"=>$indentedCatTree = function(TaxonomyRepository $er) {
                    return $er->getChildrenQueryBuilder();
                }))
            ->add('isActive',ChoiceType::class, array('label'=>'Vous créez: ?',

                                        'attr'=>array('class'=>'form-control is_main_cat_radio'),
                                        'choices' => array('une sous-catégorie'=>3, 'un tag' => 1,'une marque'=>2),'choices_as_values' => true
                                        ))

            ->add('description',TextareaType::class,array('label'=>'Texte de présentation de la catégorie',
                  'attr'=>array('rows'=>"6",'class'=>'form-control')))
            ->add('metaTitre',TextType::class,array('attr'=>array('class'=>'form-control')))
            ->add('metaDesc',TextareaType::class,array('attr'=>array('class'=>'form-control')))
            ->add('metaKeywords',TextareaType::class,array('attr'=>array('class'=>'form-control')))
            ->add('ico', PicsType::class);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DC\CoreBundle\Entity\Taxonomy'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'dc_adminbundle_taxonomy';
    }
}
