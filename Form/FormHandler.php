<?php
/*
 * Traitement de la validation des formulaires de type Post
 */

/**
 * Description of PostHandler
 *
 * @author marie
 */
// Déclaration de  notre namespace
namespace DC\CoreBundle\Form;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;


class FormHandler {
    // Attribut Formulaire
    protected $form;

    // Attribut requête HTTP
    protected $request;
    // Attribut entity Manager pour persister les données
    protected $em;

    // Grâce aux components récupérés, on passe au constructeur
    // les 3 éléments dont on a besoin pour traiter le form =>
    // Le form, la requête et l'entity manager
    public function __construct(Form $form, Request $request, EntityManager $em) {
        $this->form = $form;
        $this->request = $request;
        $this->em = $em;

    }

    public function process() {
        if($this->request->getMethod() == 'POST') {
            $this->form->handleRequest($this->request);
            if($this->form->isValid()) {
                $this->onSuccess($this->form->getData());
                return true;
            }
        }
        return false;
    }

    public function onSuccess($obj) {
        $this->em->persist($obj);
        $this->em->flush();
    }
}