<?php

namespace DC\CoreBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType as TType;

class TextType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre',TType::class,array('label'=>"Titre",'attr'=>array('class'=>'form-control')))
            ->add('content',TextareaType::class,array('label'=>"Contenu du texte (HTML autorisé)",'attr'=>array('class'=>'form-control','rows' => 30 )))
            ->add('metaTitre',TType::class,array('label'=>"Meta Titre",'attr'=>array('class'=>'form-control')))
            ->add('metaDesc',TextareaType::class,array('label'=>"Meta Desc",'attr'=>array('class'=>'form-control')))
            ->add('metaKeywords',TextareaType::class,array('label'=>"Meta Keywords",'attr'=>array('class'=>'form-control')))
            ->add('tags',EntityType::class,array('label'=>'Tags',
                'attr'=> array('class'=>'form-control chosen-select'),
                'class'=>"DC\CoreBundle\Entity\Taxonomy",
                'property'=>'nom',
                'multiple'=>true,
                'required'=>false))
            ->add('category',EntityType::class,array('label'=>'Catégorie principale',
                'attr'=>array('class'=>'form-control'),
                'class'=>"DC\CoreBundle\Entity\Taxonomy",
                "property"=>"nom"))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'DC\CoreBundle\Entity\Text'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'dc_adminbundle_text';
    }
}
